
import numpy as np
import dataProcessor as dp

from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import cohen_kappa_score
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn import linear_model
from sklearn.preprocessing import StandardScaler

data_processor = dp.load('preprocessing/log_intervals_normalised_tsfresh_relevant')
data_processor.select_relevant_features()

D = data_processor._data["tsfresh"]["training"].values[:, 0]
T = data_processor._data["tsfresh"]["training"].values[:, 1:]
L = np.intp(data_processor._data["source"]["label"].values)
I = data_processor._data["tsfresh"]["input"].values[:, 1:]

scaler = StandardScaler()
scaler.fit(T)
T = scaler.transform(T)
I = scaler.transform(I)

##

lucky = 6156

X_lucky = T[D == lucky, :]
L_lucky = L[D == lucky, :]

X_unlucky = T[D != lucky, :]
L_unlucky = L[D != lucky, :]

Xt, Xv, Yt, Yv = train_test_split(X_lucky, L_lucky, test_size=0.2)

model = linear_model.SGDClassifier(
    loss='hinge',
    #class_weight={0:(1.0 - (nLn / tot)), 1:(1.0 - (nLp / tot))},
    max_iter=10000,
    tol=1e-4,
    alpha=1.0,
    verbose=False,
    warm_start=False,
    n_jobs=-1
    )

model.fit(Xt, Yt)

Tp = model.predict(Xt)
Tk = cohen_kappa_score(Tp, Yt)
Ta = accuracy_score(Tp, Yt)
print("Training cohen_kappa_score = " + str(Tk))
print("Training accuracy_score = " + str(Ta))

Vp = model.predict(Xv)
Vk = cohen_kappa_score(Vp, Yv)
Va = accuracy_score(Vp, Yv)
print("Validation cohen_kappa_score = " + str(Vk))
print("Validation accuracy_score = " + str(Va))

# Up = model.predict(X_unlucky)
# Uk = cohen_kappa_score(Up, L_unlucky)
# Ua = accuracy_score(Up, L_unlucky)
# print("Validation cohen_kappa_score = " + str(Uk))
# print("Validation accuracy_score = " + str(Ua))
