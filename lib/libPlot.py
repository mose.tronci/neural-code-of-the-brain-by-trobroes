# Contains useful functions to plot the data

from old import libData as ld
import numpy as np
import matplotlib.pyplot as plt
import math


def plotTimeSeries(T, L, neuron_id=None):
    """
    Returns a pyplot object with a time series from a given Training Data set.
    Prints the active state in red and the non-active state in blue.

    Args:
        T(numpy.ndarray): a matrix containing the training data.
        L(numpy.ndarray): a matrix containing the target (label) data.
        neuron_id(int)   : an optional neuron id. If empty a random ID is selected.

    Returns:
        pTS: pyplot object with a time series graph: red=active, blue=non-active.
    """
    if neuron_id is None:
        neuron_id=T["neuron_id"]
        title = 'Time series for all neurons'
    else:
        title = 'Time series for neuron {}'.format(neuron_id)

    pTS = plt.title(title)
    plt.plot(T["time_series"][np.logical_and(T["neuron_id"] == neuron_id, L["label"] == 1),:].transpose(), 'r-')
    plt.plot(T["time_series"][np.logical_and(T["neuron_id"] == neuron_id, L["label"] == 0),:].transpose(), 'b:')
    plt.xlabel("Sample number")
    plt.ylabel("Time value")
    return pTS


def plotIntervalSeries(T, L, neuron_id=None):
    """
    Returns a pyplot object with an interval series from a given Training Data set.
    Prints the active state in red and the non-active state in blue.

    Args:
        T(numpy.ndarray): a matrix containing the training data.
        L(numpy.ndarray): a matrix containing the target (label) data.
        neuron_id(int)   : an optional neuron id. If empty all the Neurons are selected

    Returns:
        pTS: pyplot object with a time series graph: red=active, blue=non-active.
    """

    if neuron_id is None:
        neuron_id=T["neuron_id"]
        title = 'Interval series for all neurons'
    else:
        title = 'Interval series for neuron {}'.format(neuron_id)

    pTS = plt.title(title)
    plt.plot(ld.calculateIntervals(T["time_series"])[np.logical_and(T["neuron_id"] == neuron_id, L["label"] == 1),:].transpose(), 'r-')
    plt.plot(ld.calculateIntervals(T["time_series"])[np.logical_and(T["neuron_id"] == neuron_id, L["label"] == 0),:].transpose(), 'b:')
    plt.xlabel("Sample number")
    plt.ylabel("Interval value")
    return pTS

# Returns a pyplot object with an instogram
# matrice time series
# matrice label
# optional neuron
#
# return plt
# def intervalInstogram()
# histtype step
# def


def plotHistogram(T, L, type, neuron_id=None, num_bins=512):
    """
    Returns an histogram of the time series
    Prints the active state in red and the non-active state in blue.

    Args:
        T(numpy.ndarray): a matrix containing the training data.
        L(numpy.ndarray): a matrix containing the target (label) data.
        neuron_id(int)  : an optional neuron id. If empty all the Neurons are selected
        num_bins        : an optional parameter for the number of bins in the histogram
        type(string)    : the type of diagram, can be one of the following:
                            time for an histogram of time values
                            int for an histogram of interval values
                            log_time for an histogram of the logarithmic of time values
                            log_int for an histogram of the logarithmic of interval values

    Returns:
        pTS: pyplot object with an Histogram: red=active, blue=non-active.
    """

    plot_titles = {
        'time'      : 'Time values',
        'int'       : 'Interval values',
        'log_time'  : 'Log of time values',
        'log_int'   : 'Log  of interval values'
    }

    if neuron_id is None:
        neuron_id=T["neuron_id"]
        title = plot_titles[type]+' (all neurons)'
    else:
        title = plot_titles[type]+' (for neuron {})'.format(neuron_id)

    pTS = plt.title(title)

    if type == 'time':
        data = ld.loadTrainingData()['time_series']
    elif type == 'int':
        data = ld.calculateIntervals(ld.loadTrainingData()['time_series'])
    elif type == 'log_time':
        data = ld.calculateLogTimeseries(ld.loadTrainingData()['time_series'])
    elif type == 'log_int':
        data = ld.calculateLogIntervals(ld.loadTrainingData()['time_series'])

    data_active = data[np.logical_and(T["neuron_id"] == neuron_id, L["label"] == 1), :]
    data_non_active = data[np.logical_and(T["neuron_id"] == neuron_id, L["label"] == 0), :]

    plt.hist(np.reshape(data_active, data_active.size), num_bins, color='red', histtype='step', normed=True)
    plt.hist(np.reshape(data_non_active, data_non_active.size), num_bins, color='blue', histtype='step', normed=True)

    return pTS


def hpt_search_table_plot(hpt_clf,
                          search_description,
                          graph=True,
                          display_all_params=True,
                          filename=None,
                          show=True):

    """Display grid search results
    Args:
    ---------

    hpt_clf             the hyper parameters tuned estimator resulting from a
                        grid search or from a randomized search

    search_description  a description of the search

    param_name          a string with the name of the parameter being tested

    num_results         an integer indicating the number of results to display
                        Default: 15

    graph               boolean: should a graph be produced?
                        non-numeric parameters (True/False, None) don't graph well
                        Default: True

    display_all_params  boolean: should we print out all of the parameters, not just the ones searched for?
                        Default: True

    Usage
    -----
    hpt_search_table_plot(grid_clf, "min_samples_leaf")
    """
    from matplotlib import pyplot as plt
    import pandas as pd

    clf = hpt_clf.best_estimator_
    clf_params = hpt_clf.best_params_
    clf_score = hpt_clf.best_score_
    clf_stdev = hpt_clf.cv_results_['std_test_score'][hpt_clf.best_index_]
    cv_results = hpt_clf.cv_results_

    print("best parameters: {}".format(clf_params))
    print("best score:      {:0.5f} (+/-{:0.5f})".format(clf_score, clf_stdev))
    if display_all_params:
        import pprint
        pprint.pprint(clf.get_params())

    if graph:
        plt.figure(1)
        plt.suptitle("Hyper-parameters search results - {}".format(search_description))

        # have 3 plots per row, maximum
        num_rows = math.ceil(len(hpt_clf.best_params_) / 3)
        num_columns = max([3, math.floor(len(hpt_clf.best_params_)/3)])
        layout_base = (num_rows * 100) + (num_columns * 10)

        i = 1
        for param_name in hpt_clf.best_params_.keys():
            scores_df = pd.DataFrame(cv_results).sort_values(by='param_' + param_name)
            means = scores_df['mean_test_score']
            stds = scores_df['std_test_score']

            params = scores_df['param_' + param_name]
            # Convert the value into strings otherwise the errorbar may not plot
            index = 0
            while index < np.alen(params):
                params[index] = str(params[index])
                index += 1

            best_row = scores_df.iloc[0, :]
            best_mean = best_row['mean_test_score']
            best_stdev = best_row['std_test_score']
            best_param = best_row['param_' + param_name]

            plt.subplot(layout_base + i)

            plt.errorbar(params, means, yerr=stds)

            plt.axhline(y=best_mean + best_stdev, color='red')
            plt.axhline(y=best_mean - best_stdev, color='red')
            plt.plot(best_param, best_mean, 'or')

            plt.title(param_name + " vs Score\nBest Score {:0.5f}".format(clf_score))
            plt.xlabel(param_name)
            plt.ylabel('Score')
            i = i + 1

        plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9, wspace=0.8, hspace=0.4)

        # Save the file if a filename was provided
        if filename:
            plt.gcf().set_size_inches(18.5, 10.5)
            plt.savefig(filename + '.png')
            print('Graph for hyperparamaters search saved in file {}.png'.format(filename))

        # Show the plot if the boolean show was set to true
        if show:
            plt.show()

def willSmithPlot(F, Y, B = 128):

    X = F.values

    for n in np.arange(X.shape[1]):
        c, b = np.histogram(X[:, n], bins = B, density = True)
        ce = b[0:(len(b) - 1)] + 0.5 * np.diff(b)
        plt.plot(ce, c, "g")
        c1, _ = np.histogram(X[Y == 1.0, n], bins = b, density = True)
        c0, _ = np.histogram(X[Y == 0.0, n], bins = b, density = True)
        plt.plot(ce, c1, "r")
        plt.plot(ce, c0, "b")
        plt.title(F.columns[n])
        plt.show()
        plt.figure(n)


def willLogSmithPlot(F, Y, B = 128):

    X = np.log(F.values)

    I = np.array([41, 58, 61, 65, 70, 71, 73, 75, 76, 78, 79, 84, 86, 88, 89, 91, 92, 97, 103, 133, 134, 135, 136, 137, 140, 176, 177, 178, 180, 181, 183])

    for n in I:
        r = np.logical_not(np.logical_or(np.isinf(X[:, n]), np.isnan(X[:, n])))
        c, b = np.histogram(X[r, n], bins = B, density = True)
        ce = b[0:(len(b) - 1)] + 0.5 * np.diff(b)
        plt.plot(ce, c, "g")
        c1, _ = np.histogram(X[np.logical_and(Y == 1.0, r), n], bins = b, density = True)
        c0, _ = np.histogram(X[np.logical_and(Y == 0.0, r), n], bins = b, density = True)
        plt.plot(ce, c1, "r")
        plt.plot(ce, c0, "b")
        plt.title(F.columns[n])
        plt.show()
