import os
import warnings
from time import localtime, strftime

class Logger:
    """
    This class contains methods to:
    - Stream messages to the standard output
    - Stream messages to a log files
    """

    def __init__(self, log_file_name):
        """
        Initialises the logger using the flags and attributes passed.
        """
        self.stream_to_stdout = False
        self.listen_from_std_out = False

        if os.path.isfile(log_file_name):
            warnings.warn(log_file_name + ' already existing in file system')

        self.log_file_obj = open(log_file_name, 'a')

        self.log_file_name = log_file_name
        self.stream_to_logfile = True

        self.log_file_obj.write('Log file created at ' + self._get_time_str() + '\n')
        self.log_file_obj.flush()

    def _get_time_str(self):
        return strftime("%Y%m%d %H:%M:%S", localtime())

    def close(self):
        self.log_file_obj.close()
        self.log_file_obj = None

    def open(self):
        self.log_file_obj = open(self.log_file_name, 'a')

    def set_stdout_stream(self, flag):
        self.stream_to_stdout = flag

    def submit(self, log_string):
        log_str = self._get_time_str() + ' > ' + log_string

        if self.stream_to_stdout:
            print(log_str)

        if self.stream_to_logfile:
            self.log_file_obj.write(log_str + '\n')
            self.log_file_obj.flush()
