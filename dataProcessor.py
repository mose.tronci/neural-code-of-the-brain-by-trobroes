import pandas as pd
import numpy as np
import pickle as pkl
import time
import datetime
from tsfresh import select_features
from tsfresh.utilities.dataframe_functions import impute
from tsfresh.feature_extraction import EfficientFCParameters,ComprehensiveFCParameters, extract_features
from lib import logger
from time import gmtime, strftime
import os
from os import listdir
import sys
import itertools
import multiprocessing
from sklearn.preprocessing import StandardScaler


def load(folder_name,stream_to_log_file=False):
    """
    It creates and returns an instance of type DataProcessor reading from the pickle file specified in filename.
    :param folder_name: the folder to load from
    :return: an object of type DataProcessor
    """
    # todo: try catch the filename for non existance

    # read all the parameters
    file = open(folder_name+'/export.pkl', 'rb')
    parameters = pkl.load(file)
    file.close()
    # create a data processor and set all the parameters as per the pickle file
    preproc_description = '_'.join(folder_name.split('_')[4:]).strip('/')
    loaded_data_processor = DataProcessor(preproc_description=preproc_description, stream_to_logfile=stream_to_log_file)

    # todo: this should be done with an iterator and a specific method
    loaded_data_processor._logger.submit('Initialising parameters from {}'.format(folder_name))
    loaded_data_processor.test_mode = parameters['test_mode']
    loaded_data_processor.sample_test_size = parameters['sample_test_size']
    loaded_data_processor.pipeline_type = parameters['pipeline_type']
    loaded_data_processor.ts_fresh = parameters['ts_fresh']
    loaded_data_processor.normalise = parameters['normalise']
    loaded_data_processor.time_conv_fs = parameters['time_conv_fs']
    loaded_data_processor.time_conv_align_first = parameters['time_conv_align_first']
    loaded_data_processor.verbose = parameters['verbose']
    loaded_data_processor.n_jobs = parameters['n_jobs']
    loaded_data_processor._logger.submit('Loading data')

    for file in listdir(folder_name):
        if file.endswith('.csv'):
            data_index = file.split('_')
            index_1 = data_index[0]
            index_2 = data_index[1].strip('.csv')
            # todo: this should be done with a class instance method
            loaded_data_processor._logger.submit('Loading file {} in self._data[{}][{}]'.format(file, index_1, index_2))
            loaded_data_processor._data[index_1][index_2] = pd.read_csv(folder_name+'/'+file)
            loaded_data_processor._data[index_1][index_2].set_index('ID', inplace=True)

    loaded_data_processor._logger.submit('Data processor load complete')
    return loaded_data_processor


def run_all(list_pipeline_type=['peak_times', 'intervals', 'log_intervals', 'time_reconstruction'],
            list_tsfresh=[False],
            list_normalise=[False],
            sample_test_size=2,
            test_mode=True):
    """
    This methods executes all the combinations of preprocessing given in the parameters.
    :param list_pipeline_type: a list of pipeline_type values
    :param list_tsfresh: True or False
    :param list_normalise: True or False
    :param sample_test_size: how big the sample test size is
    :param test_mode: whether the run all should run in test mode or full mode (True or False)
    :return:
    """

    permutation = list(itertools.product(list_pipeline_type, list_tsfresh, list_normalise))

    for p in permutation:
        preproc_description='{}_tsfresh-{}_norm-{}'.format(p[0],p[1],p[2])
        mydp = DataProcessor(sample_test_size=sample_test_size,
                             preproc_description=preproc_description,
                             test_mode=test_mode)
        mydp.pipeline_type = p[0]
        mydp.ts_fresh = p[1]
        mydp.normalise = p[2]
        mydp.transform_data()
        mydp.save()


class DataProcessor:
    """
    This class contains methods to:
    - Create a new pipeline by loading source files (input and training)
    - Apply a set of transformation to the files, including:
        - Transform the pipeline type, keeping time peak, creating (log) intervals, or reconstructing time profiles
        - Calculates additional features using the TS fresh library
        - Normalises the features
    - Saves the features created so that they can be used in training
    """

    filepath_training_data = 'data/training_un7FCmp.csv'
    filepath_target_data = 'data/target_ATndViR.csv'
    filepath_input_data = 'data/input_test_YW07KeM.csv'
    filepath_preprocessing = 'preprocessing/'

    def __init__(self,
                 preproc_description,
                 test_mode=True,
                 sample_test_size=2,
                 pipeline_type='peak_times',
                 tsfresh=False,
                 tsfresh_relevant=False,
                 normalise=False,
                 time_conv_fs=1000,
                 time_conv_align_first=False,
                 verbose=True,
                 n_jobs=None,
                 stream_to_logfile=True
                 ):
        """
        Initialises an instance of the DataProcessor, which can be further manipulated using the class methods

        The pipeline is hard-coded to perform (optional) transformations in this (optional) order:
            - Performs a pipeline_type transformation
            - Calculates TS Fresh features
            - Normalises the features

        Args:
            test_mode:              allows to test with a smaller data set

            sample_test_size:       used only if test_mode = True. indicates how big the sample test size is.
                                    Defaulted to 10 if not provided

            pipeline_type:          this sets the type of pipeline, it can be one of the following:
                                    'peak_times':           keeps the original data, which is the time when
                                                            the peaks occur
                                     'intervals':           calculates the interval between the peak times,
                                                            results in a Cols-1 training set
                                     'log_intervals':       same as intervals, but calculates the log of the values
                                     'time_reconstruction': constructs a time series which emulates the spikes on
                                                            a time / amplitude, it uses the optional parameters
                                                            time_conv_fs and time_conv_align_first
                                    Data is always saved into data_transformed['pipeline']

            tsfresh:               this indicates whether TS_Fresh features must be calculated. If so, the method
                                    transform_training_and_input data updates data_transformed['tsfresh']

            tsfresh_relevant:       if true, only relevant features are selected

            normalise:              indicates whether the features should be normalised. If so, the method
                                    transform_training_and_input data updates data_transformed['normalised']

            time_conv_fs:           this is the sample rate (fs) used in time_reconstruction

            time_conv_align_first:  set to True if the first peak must be set to 0

            preproc_description:    the description of this pipeline. Do not use spaces as this is used to generate a
                                    folder with logs and export data

            verbose:                True of False flag setting the verbosity level

            n_jobs:                 number of jobs to be used for the features extract
        """

        # saves the parameters received as instance variables
        self.test_mode = test_mode
        self.sample_test_size = sample_test_size
        self.pipeline_type = pipeline_type
        self.ts_fresh = tsfresh
        self.tsfresh_relevant = tsfresh_relevant
        self.normalise = normalise
        self.time_conv_fs = time_conv_fs
        self.time_conv_align_first = time_conv_align_first
        self.preproc_description = preproc_description
        self.verbose = verbose

        # Define the allowed pipeline types for error checking
        self._pipeline_types = ['peak_times', 'intervals', 'log_intervals', 'time_reconstruction']

        # setting up target directory and logger object
        # todo: give the option not to create a folder on load
        if preproc_description is None:
            raise Exception('You must provide a value for the parameter preproc_description')
        else:
            self.filepath_self_save = \
                DataProcessor.filepath_preprocessing + \
                strftime("%Y%m%d_%H_%M_%S_", gmtime()) + \
                preproc_description
            if stream_to_logfile:
                os.mkdir(self.filepath_self_save)
            self._logger = logger.Logger(log_file_name=self.filepath_self_save + '/messages.log', stream_to_logfile=stream_to_logfile)
            self._logger.submit('Target directory {} created'.format(self.filepath_self_save))

        # setting up the n_jobs parameter
        if n_jobs is None:
            self.n_jobs = multiprocessing.cpu_count()
            self._logger.submit('Initialising n_jobs to {}, read from cpu_count()'.format(self.n_jobs))
        else:
            self.n_jobs = n_jobs
            self._logger.submit('Initialising n_jobs to {}, from calling parameter'.format(self.n_jobs))

        # Read ALL the source data, keep it in these variables so that the data['source'] can be re-sampled
        self._logger.submit('Reading all the source data in internal structures')
        self._data_source_training = pd.read_csv(DataProcessor.filepath_training_data, delimiter=',', header=0)
        self._data_source_input = pd.read_csv(DataProcessor.filepath_input_data, delimiter=',', header=0)
        self._data_source_label = pd.read_csv(DataProcessor.filepath_target_data, delimiter=',', header=0)

        # Set the index to the ID column
        self._data_source_training.set_index('ID', inplace=True)
        self._data_source_input.set_index('ID', inplace=True)
        self._data_source_label.set_index('ID', inplace=True)

        # Initialise the data to empty dictionaries
        self._data = {
            'source': {
                'training': None,
                'input':    None,
                'label':    None
            },
            'pipeline': {
                'training': None,
                'input':    None
            },
            'tsfresh': {
                'training': None,
                'input':    None
            },
            'final': {
                'training': None,
                'input':    None
            },
            'normalised': {
                'training': None,
                'input': None
            }
        }

        # set the source data according to the test of she sample_test_size
        self.set_sample_size(self.sample_test_size)

        self._logger.set_stdout_stream(verbose)

    def get_parameters(self):
        dict_parameters = {}
        for i in self.__dict__.keys():
            if not i.startswith('_'):
                dict_parameters.update({i: self.__dict__[i]})  # skip internal variables
        return dict_parameters

    def print_parameters(self):
        dict_parameters = self.get_parameters()
        for p in dict_parameters:
            self._logger.submit('[{}]: {}'.format(p, dict_parameters[p]))

    def set_sample_size(self, sample_test_size):
        """
        Resets the sample source data as per the sample_size parameters.
        It updates the self.data['source'] dictionary in place.

        :param sample_test_size: the number of rows to read from the source data
        """
        if self.test_mode:
            self.sample_test_size = sample_test_size
            self._logger.submit('Setting the sample test size to {}'.format(self.sample_test_size))
            self._data['source']['training'] = self._data_source_training.iloc[0:self.sample_test_size, :]
            self._data['source']['input']    = self._data_source_input.iloc[0:self.sample_test_size, :]
            self._data['source']['label']    = self._data_source_label.iloc[0:self.sample_test_size, :]
        else:
            self._logger.submit("The test flag is set to false, keeping all the data in data['source]'")
            self._data['source']['training'] = self._data_source_training
            self._data['source']['input']    = self._data_source_input
            self._data['source']['label']    = self._data_source_label

    def save(self, filename='export.pkl', close_log=True):
        self._logger.submit('Saving all attributes in a pickle file')
        file = open(self.filepath_self_save+'/'+filename, 'wb')
        pkl.dump(self.get_parameters(), file)
        file.close()

        self._logger.submit('Saving the data in multiple .csv files')
        for d in self._data:
            for i in self._data[d]:
                if self._data[d][i] is None:
                    continue
                self._data[d][i].to_csv(self.filepath_self_save+'/'+d+'_'+i+'.csv')
        if close_log: self._logger.close()

    def transform_data(self, verbose=True):
        """This method kicks off the transformation pipeline as per the pipeline parameters.
        The data is manipulated in place in the internal variable self._data
        """
        self._logger.set_stdout_stream(verbose)

        self._logger.submit('Commencing data transformation')
        self.print_parameters()
        self._logger.submit('Transforming the pipeline in {}'.format(self.pipeline_type))
        self.transform_pipeline_type(verbose)

        if self.ts_fresh is False:
            self._logger.submit('Skipping tsfresh transform, as the flag is set to false')
        else:
            self._logger.submit('Executing the tsfresh transform')
            self.transform_tsfresh(verbose)

        if self.normalise is False:
            self._logger.submit('Skipping normalisation as the flag is set to false')
        else:
            self._logger.submit('Executing the normalisation ')
            self.transform_normalise(verbose)

        self._logger.submit("Saving the data in _data['final']")
        if self.ts_fresh is False and self.normalise is False:
            self._data['final']['training'] = self._data['pipeline']['training']
            self._data['final']['input']    = self._data['pipeline']['input']
        elif self.ts_fresh is True and self.normalise is False:
            self._data['final']['training'] = self._data['tsfresh']['training']
            self._data['final']['input']    = self._data['tsfresh']['input']
        elif self.ts_fresh is False and self.normalise is True:
            self._data['final']['training'] = self._data['normalised']['training']
            self._data['final']['input']    = self._data['normalised']['input']
        elif self.ts_fresh is True and self.normalise is True:
            self._data['final']['training'] = self._data['normalised']['training']
            self._data['final']['input'] = self._data['normalised']['input']

    def transform_pipeline_type(self, verbose=True, finalise=False):  # todo: add the finalisation logic, with a method
        """
        Transforms the data according to the pipeline type.
        All the changes are done in place in the instance dictionary:
            data_transformed['pipeline']['training']
            data_transformed['pipeline']['input']

        Inspects the parameter 'pipeline_type', which can take the following values:
                'peak_times', 'intervals', 'log_intervals', 'time_reconstruction'

        """
        self._logger.set_stdout_stream(verbose)

        start_time = time.time()
        self._logger.submit('*** Start {} transformation ***'.format(self.pipeline_type))
        if self.pipeline_type not in self._pipeline_types:
            raise Exception("'pipeline_type' must be one of {}".format(self._pipeline_types))

        if self.pipeline_type == 'peak_times':
            self._data['pipeline']['training'] = \
                self._data['source']['training']
            self._data['pipeline']['input'] = \
                self._data['source']['input']

        elif self.pipeline_type == 'intervals':
            self._data['pipeline']['training'] = \
                self._transform_data_to_intervals(self._data['source']['training'])
            self._data['pipeline']['input'] = \
                self._transform_data_to_intervals(self._data['source']['input'])

        elif self.pipeline_type == 'log_intervals':
            self._data['pipeline']['training'] = \
                self._transform_data_to_intervals(self._data['source']['training'], logaritmic=True)
            self._data['pipeline']['input'] = \
                self._transform_data_to_intervals(self._data['source']['input'], logaritmic=True)

        elif self.pipeline_type == 'time_reconstruction':
            self._data['pipeline']['training'] = \
                self._transform_data_to_time_reconstruction(
                    self._data['source']['training'], data_type='training')
            self._data['pipeline']['input'] = \
                self._transform_data_to_time_reconstruction\
                    (self._data['source']['input'], data_type='input')

        end_time = time.time()
        self._logger.submit('*** {} time complete! '
                            'Start time = {}, '
                            'End time = {}, '
                            'Total time = {}s ***'.format(
                                    self.pipeline_type,
                                    datetime.datetime.fromtimestamp(start_time).strftime("%H:%M:%S on %d %b"),
                                    datetime.datetime.fromtimestamp(end_time).strftime("%H:%M:%S on %d %b"),
                                    round(end_time-start_time, 2)
                                )
                            )

    def transform_tsfresh(self, verbose=True, finalise=False):
        """
        Transforms the data according to the tsfresh flag.
        All the changes are done in place in the instance dictionary:
            data_transformed['tsfresh']['training']
            data_transformed['tsfresh']['input']

        Inspects the parameter 'tsfresh', which can take the following values:
            True: the transform is executed
            False: the transform is not executed and the object is left at none
        """
        self._logger.set_stdout_stream(verbose)

        # Call _extract_tsfresh on training
        self._data['tsfresh']['training'] = \
            self._extract_tsfresh(self._data['pipeline']['training'])
        self._logger.submit('Features calculated on training set')

        # Call _extract_tsfresh on input
        self._data['tsfresh']['input'] = \
            self._extract_tsfresh(self._data['pipeline']['input'])
        self._logger.submit('Features calculated on input set')

        # Check that the two have exactly the same structure, otherwise print an exception
        comparison = \
            (self._data['tsfresh']['training'].columns ==
             self._data['tsfresh']['input'].columns)
        if comparison.tolist().count(False) > 0:
            raise Exception('The structure of the transformed input and training data are different')

        # if tsfresh_relevant is True, run the method
        if self.tsfresh_relevant:
            self.select_relevant_features()

        if finalise: # todo: add the finalisation logic, with a method
            self._logger.submit("Finalise is set to True, saving the data in _data['final']")
            self._data['final']['training'] = self._data['tsfresh']['training']
            self._data['final']['input'] = self._data['tsfresh']['input']

    def transform_normalise(self, verbose=True):
        """
        Normalises the features according to the self.normalise parameter
        All the changes are done in place in the instance dictionary:
            data_transformed['normalise']['training']
            data_transformed['normalise']['input']

        Inspects the parameter 'normalise', which can take the following values:
            True: the feature normalisation is executed
            False: the transform is not executed and the object is left at none

        If the tsfresh flag is set to True, the normalisation is applied to data['tsfresh'], otherwise is
        applied to data['pipeline']
        """
        self._logger.set_stdout_stream(verbose)
        scaler = StandardScaler()

        if self.ts_fresh is True:
            self._logger.submit("Running normalisation on data['tsfresh']['training']")
            normalised_training     = self._data["tsfresh"]["training"].values[:, 1:]
            normalised_input        = self._data["tsfresh"]["input"].values[:, 1:]
            scaler.fit(normalised_training)
            self._data["normalised"]["training"]            = self._data["tsfresh"]["training"].copy(deep=True)
            self._data["normalised"]["training"].loc[:, 1:] = scaler.transform(normalised_training)
            self._data["normalised"]["input"]               = self._data["tsfresh"]["input"].copy(deep=True)
            self._data["normalised"]["input"].loc[:, 1:]    = scaler.transform(normalised_input)
            self._logger.submit("Results saved in ['normalised']['training'] and ['normalised']['input'] ")

        else:
            self._logger.submit("Running normalisation on data['pipeline']['training']")
            normalised_training     = self._data["pipeline"]["training"].values[:, 1:]
            normalised_input        = self._data["pipeline"]["input"].values[:, 1:]
            scaler.fit(normalised_training)
            self._data["normalised"]["training"]            = self._data["pipeline"]["training"].copy(deep=True)
            self._data["normalised"]["training"].loc[:, 1:] = scaler.transform(normalised_training)
            self._data["normalised"]["input"]               = self._data["pipeline"]["input"].copy(deep=True)
            self._data["normalised"]["input"].loc[:, 1:]    = scaler.transform(normalised_input)
            self._logger.submit("Results saved in ['normalised']['training'] and ['normalised']['input'] ")


    def select_relevant_features(self):
        """
        Selects the relevant features from TS Fresh, and updates the self._data['final'] structures in place
        """


        relevant_features = select_features(
            impute(self._data['tsfresh']['training']),
            self._data['source']['label'].values.ravel()
        )
        self._logger.submit("{} out of {} features have predictive power".format(
            relevant_features.columns.size,
            self._data['tsfresh']['training'].columns.size
            )
        )

        self._data['tsfresh']['training'] = pd.concat([
            self._data['tsfresh']['training']['neuron_id'],
            self._data['tsfresh']['training'][relevant_features.columns]],
            axis = 1
        )
        self._data['tsfresh']['input'] = pd.concat([
            self._data['tsfresh']['input']['neuron_id'],
            self._data['tsfresh']['input'][relevant_features.columns]],
            axis = 1
        )
        self._logger.submit("Relevant features overwritten in the self._data['tsfresh'] structures")


    def _aggregate(self, data, mean=True, var=True, min=True, max=True):
        """
        Aggregates the final data set by neuron_id

        :return: the data aggregated by neuron_id
        """
        data = self._stack_data(data, 'intervals')
        data.set_index('ID', inplace=True)
        if mean:
            data_mean= data.groupby(['neuron_id']).mean()
            data_mean.rename(columns={'intervals': 'mean'}, inplace=True)
        if var:
            data_var= data.groupby(['neuron_id']).var()
            data_var.rename(columns={'intervals': 'var'}, inplace=True)
        if min:
            data_min= data.groupby(['neuron_id']).min()
            data_min.rename(columns={'intervals': 'min'}, inplace=True)
        if max:
            data_max = data.groupby(['neuron_id']).max()
            data_max.rename(columns={'intervals': 'max'}, inplace=True)

        data_return = pd.concat([data_mean, data_var, data_min, data_max], axis=1)

        return data_return


    def _init_normalisation_parameters(self, data):
        """Initialisaes the normalisation parameters. The same parameters must be used across training and input"""
        self.normalisation_parameters = {
            'mean'  : data.set_index('neuron_id', append=True).mean(),
            'var'   : data.set_index('neuron_id', append=True).var()}

    def _apply_normalisation(self, data):
        if self.normalisation_parameters is None:
            raise Exception('You must initialise the normalisation parameters before applying normalisation')

        normalised_data = data.copy(deep=True)
        normalised_data.set_index('neuron_id', append=True, inplace=True)
        normalised_data = (normalised_data - self.normalisation_parameters['mean']) / self.normalisation_parameters['var']
        normalised_data.reset_index('neuron_id', inplace=True)
        return normalised_data


    def _timestamps2timeseries(self, intervals, Fs, align, max_time):
        #todo: this needs to return always the same structure, otherwise it's messy to save the stream in a pandas
        if align:
            intervals = intervals - intervals[0]

        if max_time is None:
            max_time = np.ceil(np.max(intervals))

        n_samples = np.intp(np.floor(max_time * Fs))
        n_nyquist = np.floor(n_samples / 2.0) + 1.0
        intervals_samples = intervals * Fs
        norm_freq = np.arange(n_nyquist) / n_samples
        core_matrix = norm_freq.reshape(1, len(norm_freq)) * intervals_samples.reshape(len(intervals_samples), 1)

        time_series_fft = np.sum(np.exp(-1j * 2.0 * np.pi * (core_matrix - np.floor(core_matrix))), axis=0)

        time_series = np.fft.irfft(time_series_fft, n_samples)

        return_data = pd.DataFrame.from_records(time_series.reshape((1, len(time_series))))
        return_data = return_data.add_prefix('sample_')

        return return_data

    def _stack_data(self, data, column_name='time_series'):
        """Stacks the data

        :return: a dataframe with the data stacked into the given column name"""
        data_stacked = data.copy(deep=True)
        # set the indices to ID and neuron_id so that can stack the data afterwards
        data_stacked.set_index('neuron_id', append=True, inplace=True)
        # stacks the data: creates a level_2 column with the original col names, and a 0 col with the timestamp
        data_stacked = data_stacked.stack().to_frame()
        # get rid of the indexes, drop the level 2, and rename 0 to column_name
        data_stacked.reset_index(inplace=True)
        data_stacked.drop(columns=['level_2'], inplace=True)
        data_stacked.rename(index=str, columns={0: column_name}, inplace=True)

        return data_stacked

    def _extract_tsfresh(self, data):
        """
        This method creates additional features using the TS Fresh library. It only uses the EfficientFCParameters
        configuration of the extract_features method, to keep computational complexity down.

        Args:
            data: a pandas dataframe with the data that needs to be used to extract features on

        Returns:
           data_tsfresh_features: a pandas data frame with the extracted features
        """

        start_time = time.time()
        self._logger.submit('*** Start tsfresh ***')

        if self.ts_fresh: # todo: the if should move to the caller
            data_keys = data.reset_index()
            data_keys = data_keys[['ID', 'neuron_id']]
            data_stacked = self._stack_data(data)

            # now extract the features
            data_tsfresh_features = extract_features(
                data_stacked,
                column_id='ID',
                column_value='time_series',
                default_fc_parameters=ComprehensiveFCParameters(),
                n_jobs = self.n_jobs
            )
            self._logger.set_stdout_stream(False) #todo: it does not work!!!

        # add the ID and the neuron id back to the dataframe
        self._logger.submit('Adding ID and neuron_id to the ts fresh dataframe')
        # reset the indexes so that the rows join together when doing concat
        data_keys.reset_index(inplace=True)
        data_tsfresh_features.reset_index(inplace=True)
        data_tsfresh_features = pd.concat([
            data_keys[['ID', 'neuron_id']],
            data_tsfresh_features], axis=1)
        data_tsfresh_features.set_index('ID', inplace=True)
        data_tsfresh_features.drop('id', axis=1, inplace=True)

        end_time = time.time()
        self._logger.submit('*** TS Fresh complete! '
                            'Start time = {}, '
                            'End time = {}, '
                            'Total time = {}s ***'.format(
                                    datetime.datetime.fromtimestamp(start_time).strftime("%H:%M:%S on %d %b"),
                                    datetime.datetime.fromtimestamp(end_time).strftime("%H:%M:%S on %d %b"),
                                    round(end_time-start_time, 2)
                                )
                            )

        # data_tsfresh_features.reset_index(inplace=True) todo: this was removed for debugging
        return data_tsfresh_features

    def _transform_data_to_intervals(self, data, logaritmic=False):
        """
        Internal method to create a intervals pipeline
        :param data: the data to process
        :param logaritmic: True or False, whether the method should return logaritmic intervals or not
        :return: a DataFrame with the time intervals, with the index set to ID
        """
        data_transformed = data.copy(deep=True)
        data_transformed.set_index('neuron_id', append=True, inplace=True)
        data_transformed = data_transformed.diff(axis=1)
        if logaritmic:
            data_transformed = data_transformed.apply(func=lambda x: np.log(x))
        # drop the first column as it has NaN values (intervals start between column 1 and 0)
        data_transformed.drop(data_transformed.columns[0], axis=1, inplace=True)
        # rename the columns into intervals
        data_transformed.rename(columns=lambda x: 'interval_' + str(int(x.split('_')[1]) - 1), inplace=True)
        data_transformed.reset_index('neuron_id', inplace=True)

        return data_transformed

    def _transform_data_to_time_reconstruction(self, data, data_type):
        start_time = time.time()
        file_stream = self.filepath_self_save + "/time_reconstruction_stream_" + data_type + '.tmp'
        file = open(file_stream, "w+")

        # save the ID and neuron ID
        data_keys = data.reset_index()
        data_keys = data_keys[['ID', 'neuron_id']]

        is_init = False
        max_time = np.ceil(np.max(data.filter(regex='^timestamp', axis=1).values))  # samples forced to be the same size
        total_records = data.shape[0]

        for e in np.arange(data.shape[0]):
            iteration_start_time = time.time()

            # self._logger.submit('{} example {} of {}'.format(data_type, e+  1, data.shape[0]))
            p_train_series = self._timestamps2timeseries(
                data.filter(regex='^timestamp', axis=1).values[e, :],
                self.time_conv_fs,
                self.time_conv_align_first,
                max_time=max_time
            )

            # add the ID and the neuron ID to the front of the record
            data_transformed = pd.concat(
                [
                    pd.DataFrame(data_keys.iloc[[e], :].values, columns=['ID', 'neuron_id']), # create a new frame as the p_train has index = 0 all the times
                    p_train_series
                ],
                axis=1)
            data_transformed.set_index('ID', inplace=True)

            if file_stream: #write to the log, don't keep growing the pandas
                if not is_init:  # this is the first record, write the header as well
                    data_transformed.to_csv(file, header=True)
                    is_init = True
                else: # any other record
                    data_transformed.to_csv(file, header=False)
            data_transformed.reset_index(inplace=True)

            # Calculate and log the processing time statistics
            iteration_end_time = time.time()
            iteration_time = iteration_end_time - iteration_start_time
            mean_record_processing_time = (iteration_end_time - start_time) / (e+1)
            eta_datetime = datetime.datetime.fromtimestamp(
                start_time + (total_records * mean_record_processing_time))
            self._logger.submit('{} example {} of {}: '
                                'Processing time = {}s, '
                                'Mean record processing time = {}s, '
                                'ETA: {}'.format(
                                    data_type,
                                    e+1,
                                    total_records,
                                    round(iteration_time, 2),
                                    round(mean_record_processing_time, 2),
                                    eta_datetime.strftime("%H:%M:%S on %d %b")
                                    )
                                )
        file.close()

        # Now save the data from the file in a pandas data frame that can be returned
        return_data = pd.read_csv(file_stream, sep=',')
        return_data.set_index('ID', inplace=True)

        return return_data


