from tsfresh.examples.robot_execution_failures import download_robot_execution_failures, load_robot_execution_failures
from tsfresh import extract_features
import matplotlib.pyplot as plt
from tsfresh import select_features
from tsfresh.utilities.dataframe_functions import impute
from tsfresh import extract_relevant_features

download_robot_execution_failures()
timeseries, y = load_robot_execution_failures()
print(y.shape)

print(timeseries.head())

sensors_list=['F_x',  'F_y',  'F_z',  'T_x',  'T_y',  'T_z']

#timeseries[timeseries['id'] == 3].plot(x='time', y=sensors_list, subplots=True, sharex=True, layout=(3,2), title='Time series for id=3 (no failure)')
#timeseries[timeseries['id'] == 21].plot(x='time', y=sensors_list, subplots=True, sharex=True, layout=(3,2), title='Time series for id=21 (failure)')
#plt.show()

extracted_features = extract_features(timeseries, column_id="id", column_sort="time")
impute(extracted_features)
features_filtered = select_features(extracted_features, y)

features_filtered_direct = extract_relevant_features(timeseries, y, column_id='id', column_sort='time')

