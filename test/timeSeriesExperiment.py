from old import libData as ld
import numpy as np
import matplotlib.pyplot as plt
import time
import scipy.io.wavfile

Fs = 8000

D = ld.loadTrainingData()
X = D["time_series"]

x = X[10000, :]

T = np.ceil(np.max(X)) + 1.0

N = np.intp(np.floor(T * Fs))

xS = x * Fs
xSf = np.floor(xS)
d = xS - xSf

s = np.zeros((N, ))

s[np.intp(xSf)] = 1.0

start = time.time()

sAcc = np.zeros((N, ))

for n in np.arange(len(x)):
    spk = np.zeros((N, ))
    spk[np.intp(xSf[n])] = 1.0
    SPK = np.fft.rfft(spk)
    w = np.arange(len(SPK)) * d[n] / len(spk)
    ph = 2.0 * np.pi * (w - np.floor(w))
    sAcc = sAcc + np.fft.irfft(SPK * np.exp(-1j * ph), len(spk))

end = time.time()
print(end - start)

#plt.plot(s)
plt.plot(sAcc)
plt.show()

scipy.io.wavfile.write("spikes.wav", Fs, sAcc)
