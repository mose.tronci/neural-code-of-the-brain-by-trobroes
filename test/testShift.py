import numpy as np
import matplotlib.pyplot as plt

np.random.seed(128)

n = 2.0 * np.random.rand(10) - 1
z = np.zeros((10, ))
d = 2.5

x = np.concatenate((n, z))

X = np.fft.rfft(x)

w = np.arange(len(X)) * d / len(x)
ph = 2 * np.pi * (w - np.floor(w))
P = P = np.exp(-1j * ph)

Y = X * P

y = np.fft.irfft(Y, len(x))

plt.plot(x)
plt.plot(y)
plt.show()
