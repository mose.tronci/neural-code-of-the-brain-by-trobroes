import numpy as np
import time

# WRONG

# Fs = 1000.0
# X = 50 * np.random.rand(10, 50)
#
# maxPeakTime = np.ceil(np.max(X))
# L = np.intp(np.floor(Fs * maxPeakTime))
#
# Xs = Fs * X
# Xsi = np.floor(Xs)
# d = Xs - Xsi
#
# rawPeaks = np.zeros((X.shape[0], L))
# rIdx = np.arange(rawPeaks.shape[0])
# rows = np.repeat(rIdx.reshape(len(l), 1), Xsi.shape[1], axis=1)
# rawPeaks[rows, np.intp(Xsi)] = 1.0
#
# check = np.zeros((X.shape[0], L))
#
# for r in np.arange(X.shape[0]):
#     check[r, np.intp(Xsi[r, :])] = 1.0
#
# print(np.array_equal(rawPeaks, check))
#
# rawFFT = np.fft.rfft(rawPeaks, axis=1)
#
# nyq = np.floor(L / 2.0) + 1.0
# w = np.arange(nyq) / L

# PROTO
# Fs = 1000.0
# x = 50 * np.random.rand(50)
#
# xs = Fs * x
# xi = np.floor(xs)
# d = xs - xi
# L = np.ceil(np.max(xs))
# nyq = np.floor(L / 2.0) + 1.0
# w = np.arange(nyq) / L
#
# t0 = time.time()
#
# rawPeaks = np.zeros((len(x), np.intp(L)))
# rawPeaks[np.arange(rawPeaks.shape[0]), np.intp(xi)] = 1.0
#
# rawFFT = np.fft.rfft(rawPeaks, axis=1)
#
# M = w.reshape(1, len(w)) * d.reshape(len(d), 1)
# P = np.exp(-1j * 2.0 * np.pi * (M - np.floor(M)))
#
# peaks = np.fft.irfft(rawFFT * P, np.intp(L))
#
# y = np.sum(peaks, axis=0)
#
# tv = time.time()
# print(tv - t0)
#
# rawPeaks = np.zeros((len(x), np.intp(L)))
# rawPeaks[np.arange(rawPeaks.shape[0]), np.intp(xi)] = 1.0
# M = w.reshape(1, len(w)) * d.reshape(len(d), 1)
# yol = np.sum(np.fft.irfft(
#     np.fft.rfft(rawPeaks, axis=1) * np.exp(-1j * 2.0 * np.pi * (M - np.floor(M))),
#     np.intp(L)
#     ), axis=0)
#
# tol = time.time()
# print(tol - tv)
#
# print(np.array_equal(yol, y))
#
# # Check
# yt = np.zeros((np.intp(L), ))
# for t in np.arange(len(x)):
#     pt = np.zeros((np.intp(L), ))
#     pt[np.intp(xi[t])] = 1.0
#     PT = np.fft.rfft(pt)
#     mt = w * d[t]
#     HT = np.exp(-1j * 2.0 * np.pi * (mt - np.floor(mt)))
#     yt = yt + np.fft.irfft(PT * HT, np.intp(L))
#
# tl = time.time()
# print(tl - tol)
#
# print(np.array_equal(yt, y))

def prepare(Fs, N, tMax, seed=128):
    np.random.seed(seed)
    x = tMax * np.random.rand(N)

    xs = Fs * x
    xi = np.floor(xs)
    d = xs - xi
    L = np.ceil(np.max(xs))
    nyq = np.floor(L / 2.0) + 1.0
    w = np.arange(nyq) / L

    return (x, xs, xi, d, L, nyq, w)

def testVector1(Fs, N, tMax):

    out = prepare(Fs, N, tMax)

    x = out[0]
    xs = out[1]
    xi = out[2]
    d = out[3]
    L = out[4]
    nyq = out[5]
    w = out[6]

    t0 = time.time()
    rawPeaks = np.zeros((len(x), np.intp(L)))
    rawPeaks[np.arange(rawPeaks.shape[0]), np.intp(xi)] = 1.0
    rawFFT = np.fft.rfft(rawPeaks, axis=1)
    M = w.reshape(1, len(w)) * d.reshape(len(d), 1)
    P = np.exp(-1j * 2.0 * np.pi * (M - np.floor(M)))
    peaks = np.fft.irfft(rawFFT * P, np.intp(L))
    y = np.sum(peaks, axis=0)
    t1 = time.time()
    return (y, t1 - t0)

def testVector2(Fs, N, tMax):

    out = prepare(Fs, N, tMax)

    x = out[0]
    xs = out[1]
    xi = out[2]
    d = out[3]
    L = out[4]
    nyq = out[5]
    w = out[6]

    t0 = time.time()
    rawPeaks = np.zeros((len(x), np.intp(L)))
    rawPeaks[np.arange(rawPeaks.shape[0]), np.intp(xi)] = 1.0
    M = w.reshape(1, len(w)) * d.reshape(len(d), 1)
    y = np.sum(np.fft.irfft(
        np.fft.rfft(rawPeaks, axis=1) * np.exp(-1j * 2.0 * np.pi * (M - np.floor(M))),
        np.intp(L)
        ), axis=0)
    t1 = time.time()
    return (y, t1 - t0)

def testLoop(Fs, N, tMax):

    out = prepare(Fs, N, tMax)

    x = out[0]
    xs = out[1]
    xi = out[2]
    d = out[3]
    L = out[4]
    nyq = out[5]
    w = out[6]

    t0 = time.time()
    y = np.zeros((np.intp(L), ))
    for t in np.arange(len(x)):
        pt = np.zeros((np.intp(L), ))
        pt[np.intp(xi[t])] = 1.0
        PT = np.fft.rfft(pt)
        mt = w * d[t]
        HT = np.exp(-1j * 2.0 * np.pi * (mt - np.floor(mt)))
        y = y + np.fft.irfft(PT * HT, np.intp(L))
    t1 = time.time()

    return (y, t1 - t0)

def freqDomTest(Fs, N, tMax, seed):
    K = Fs * tMax
    np.random.seed(seed)
    x = K * np.random.rand(N)
    x.sort()
    nyq = np.floor(K / 2.0) + 1.0
    w = np.arange(nyq) / K
    M = w.reshape(1, len(w)) * x.reshape(len(x), 1)
    return np.sum(np.exp(-1j * 2.0 * np.pi * (M - np.floor(M))), axis=0)
