#!/bin/bash

sudo apt-get update && sudo apt-get -y upgrade
sudo apt-get install git -f -y
sudo apt-get install python3 -f -y
sudo apt-get install python3-pip -f -y
git clone https://mose.tronci:H237hjCwkFEk@gitlab.com/mose.tronci/neural-code-of-the-brain-by-trobroes.git
cd neural-code-of-the-brain-by-trobroes/
mkdir data
mkdir preprocessing
gsutil cp gs://neural-code-of-the-brain-by-trobroes/data/* ./data/
pip3 install -r requirements.pip