import dataProcessor as dp
from hyperParamsTuner import HyperParametersTuner
import numpy as np

mydp = dp.load('preprocessing/time_reconstruction_100htz_tsfresh')
data = mydp._data['tsfresh']['training']
labels = mydp._data['source']['label'].values.ravel()
input_data = mydp._data['tsfresh']['input']

hpt = HyperParametersTuner(data, labels, input_data, test_size=0.2)

model_type = 'RFC'
model_name = 'models/tsfresh-timerec100htz-randomforest-random'
search_description = 'Random Forest v01, random search on 100htz TS Fresh'
search_type = 'RFC_random'

rfc_parameters = {
    # 'criterion':          ['gini'],
    # 'max_depth':          [None],
    # 'min_samples_split':  [2],
    # 'warm_start':         [True],
    'n_estimators':         np.arange(1, 4,3),
    'max_features':         np.arange(1, 4, 3),
    # 'oob_score':          [False],
    'n_jobs':               [4],
    'bootstrap':            [False]
}

hpt.set_rfc_grid_parameters(rfc_parameters)
hpt.hyper_parameters_tune(search_type=search_type, search_description=search_description)
hpt.save_model(model_name, model_type)
hpt.generate_submission_file(model_name + '_submission', model_type)
print(hpt.rfc_fitted_classifier.best_params_)
hpt.calculate_cohen_kappa_for_best_estimator(model_type)
hpt.plot_parameters_search(model_type, graph=True, display_all_params=True, filename=model_name, show=False)