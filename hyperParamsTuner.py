import pickle
from lib import libPlot as lp
import numpy as np
import pandas as pd
#from datetime import datetime
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import cohen_kappa_score, make_scorer
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, train_test_split
from sklearn.feature_selection import SelectFromModel


class HyperParametersTuner:
    """
    This class contains helpful methods which allow to perform a hyper parameter search for Neural Network and
    Random Forest classifiers, calculate their accuracy (using a cohen_kappa score), plotting the retulst of the
    search, and ultimately generate the files ready for submission.
    """

    #Some class parameters
    #training_set_proportion = 0.6
    kappa_scorer = make_scorer(cohen_kappa_score)
    # export_file_path = 'models/'

    def __init__(self, data, labels, input_data, test_size = 0.2,
                 rfc_grid_parameters=None, rfc_random_grid=None, nn_tuned_parameters=None):
        """
        Creates an object which stores training data, training labels, and input data (to be predicted).
        Optional arguments allow to define the perimeter for the hyperparameters search:

        Args:
            data (pandas DataFrame): a pandas DataFrame containing the training data

            labels (numpy.ndarray): contains the labels data

            input_data (pandas DataFrame): contains the data for which predictions need to be generated

            test_size: the proportion of data reserved for testing expressed as a number between 0 and 1
            this is defaulted to 0.33

            rfc_grid_parameters: optional parameter specifying the perimeter for a Random forest exhaustive search

            rfc_random_grid: optional parameter specifying the perimeter for a Random forest random search

            nn_tuned_parameters: optional parameter specifying the perimeter for a Neural Network search
        """

        # Saves the data, labels and input data into instance attributes
        self.data = data
        self.labels = labels
        self.input_set = input_data

        # Split the data in a training set and a test set, as per the global class parameters
        self.test_size = test_size
        self.data_training, self.data_test, self.y_training, self.y_test = \
            train_test_split(self.data, self.labels, test_size=self.test_size)

        # Initialise the Neural Network perimeter search parameters
        if nn_tuned_parameters:
            self.nn_tuned_parameters = nn_tuned_parameters
        else:
            self.nn_tuned_parameters = {
                'hidden_layer_sizes': [
                        # (184, 95, 45, 22, 10, 5, 2),
                        # (184, 45, 10, 2),
                        # (184, 90, 30),
                        # (90, 45, 30),
                        # (184, 45),
                        # (184),
                        (789),
                        (300, 150, 5),
                        (5, 5, 2),
                        (50, 10, 2)
                ],
                # 'learning_rate': ["constant", "invscaling", "adaptive"],
                # 'activation': ["logistic", "relu", "tanh"],
                # 'alpha': 10.0 ** -np.arange(1, 10),
                # 'solver': ['lbfgs', 'sgd', 'adam'],
                'max_iter': [10000]
            }


        # Initialise the Random Forest Classifier parameters for an exhaustive search
        if rfc_grid_parameters:
            self.rfc_grid_parameters = rfc_grid_parameters
        else:
            self.rfc_grid_parameters = {
                #'criterion':            ['gini'],
                #'max_depth':            [None],
                #'min_samples_split':    [2],
                # 'warm_start':           [True]
                'n_estimators':          [7],
                'max_features':          [30, 90, 100, 110],
                'oob_score':             [False],
                'n_jobs':                [-1],
                'bootstrap':             [False]
                }

        # Initialise the Random Forest Classifier parameters for a random (sample) search
        if rfc_random_grid:
            self.rfc_random_grid = rfc_random_grid
        else:
            self.rfc_random_grid = {
                'n_estimators': range(1, 1001, 50),
                'max_features': ['auto', 'sqrt'],
                'max_depth': range(1, 401, 20),
                'min_samples_split': range(2, 202, 10),
                'min_samples_leaf': range(2, 202, 10),
                'bootstrap': [True]
                }

        # Create instance variables to store the fitter classifiers
        self.nn_fitted_classifier = None
        self.rfc_fitted_classifier = None
        self.search_description = None


    def set_rfc_rfc_random_grid(self, rfc_random_grid):
        """
        Setter for the rfc_random_grid
        Args:
            rfc_random_grid: parameter specifying the perimeter for a Random forest random search
        """
        self.rfc_random_grid = rfc_random_grid

    def set_rfc_grid_parameters(self, rfc_grid_parameters):
        """
        Setter for the set_rfc_grid_parameters
        Args:
            rfc_grid_parameters: parameter specifying the perimeter for a exhaustive Random forest search
        """
        self.rfc_grid_parameters = rfc_grid_parameters

    def set_nn_tuned_parameters(self, nn_tuned_parameters):
        """
        Setter for the nn_tuned_parameters
        Args:
            nn_tuned_parameters: parameter for the Neural Network hyperparameters search
        """
        self.nn_tuned_parameters = nn_tuned_parameters


    def hyper_parameters_tune(self, search_type, search_description):
        """
        Explores different configurations of a Random Forest Classifier as set out in the rfc_tuned_parameters
        and assigns the model to the variable self.rfc_fitted_classifier

        Args:
            search_type: one of RFC_random, RFC_exhaustive, NN_random, NN_exhaustive
            search_description: a descriptive label of the search

        Returns:

        """

        self.search_description = search_description

        if search_type=='RFC_random':
            rfc_clf = RandomizedSearchCV(
                estimator=RandomForestClassifier(),
                param_distributions=self.rfc_random_grid,
                n_jobs=-4,
                verbose=2,
                cv=10,
                scoring=HyperParametersTuner.kappa_scorer,
                return_train_score=True)
        elif search_type=='RFC_exhaustive':
            rfc_clf = GridSearchCV(
                estimator=RandomForestClassifier(),
                param_grid=self.rfc_grid_parameters,
                n_jobs=-4,
                verbose=2,
                cv=10,
                scoring=HyperParametersTuner.kappa_scorer,
                return_train_score=True)
        elif search_type=='NN_random':
            nn_classifier = RandomizedSearchCV(
                estimator=MLPClassifier(),
                param_distributions=self.nn_tuned_parameters,
                n_jobs=4,
                verbose=2,
                cv=10,
                scoring=HyperParametersTuner.kappa_scorer,
                return_train_score=True)
        elif search_type=='NN_exhaustive':
            nn_classifier = GridSearchCV(
                estimator=MLPClassifier(),
                param_grid=self.nn_tuned_parameters,
                n_jobs=-4,
                verbose=2,
                cv=10,
                scoring=HyperParametersTuner.kappa_scorer,
                return_train_score=True)
        else:
            raise Exception('search_type must be one of RFC_random, RFC_exhaustive, NN_random, NN_exhaustive')

        # Now fit the classifiers
        if search_type == 'RFC_random' or search_type=='RFC_exhaustive':
            self.rfc_fitted_classifier  = rfc_clf.fit(self.data_training, self.y_training)
        elif search_type == 'NN_random' or search_type=='NN_exhaustive':
            self.nn_fitted_classifier = nn_classifier.fit(self.data_training, self.y_training)

    def save_model(self, filename, model_type):
        #todo add filename for save
        """
        Saves the models as per the variables in self.rfc_fitted_classifier and nn.rfc_fitted_classifier in a
        pickle file, adding the .pckl extension.
        Also saves the parameters tuned in a separate file with a csv extension and suffix 'model'

        Args:
            filename: the name of the file, without extension
            model_type: the model to be saved, must be one of NN or RFC

        """
        file = open(filename+'.pckl', 'wb')

        if model_type == 'RFC':
            pickle.dump(self.rfc_fitted_classifier, file)
            cv_results = self.rfc_fitted_classifier.cv_results_
        elif model_type == 'NN':
            pickle.dump(self.nn_fitted_classifier, file)
            cv_results = self.nn_fitted_classifier.cv_results_
        else:
            raise Exception('model_type must be one of RFC, NN')

        file.close()

        scores_df = pd.DataFrame(cv_results).sort_values(by='rank_test_score')
        scores_df.to_csv(index=True, path_or_buf=filename+'_params.csv')

    def load_model(self, filename, model_type):
        """Loads the models from a given filename and assigns them to either the variable
        self.nn_fitted_classifier or self.rfc_fitted_classifier.

         Args:
            filename: the name of the file to load, with extension
            model_type: the model to be loaded, must be one of NN or RFC
        """
        file = open(filename, 'rb')
        if model_type == 'RFC':
            self.rfc_fitted_classifier = pickle.load(file)
        elif model_type == 'NN':
            self.nn_fitted_classifier = pickle.load(file)
        else:
            raise Exception('model_type must be one of RFC, NN')

        file.close()


    def calculate_cohen_kappa_for_best_estimator(self, model_type):
        """
        Calculates the cohen kappa score for the best estimator, using the test data
        Args:
            model_type: the model to be loaded, must be one of NN or RFC
        """
        if model_type == 'RFC':
            best_estimator = self.rfc_fitted_classifier.best_estimator_
        elif model_type == 'NN':
            best_estimator = self.nn_fitted_classifier.best_estimator_
        else:
            raise Exception('model_type must be one of RFC, NN')

        pred_y = best_estimator.predict(self.data_test)
        cks = cohen_kappa_score(pred_y, self.y_test)
        print("The best estimator for the {} model has a cohen kappa score of {}".format(model_type, cks))

    def generate_submission_file(self, filename, model_type):
        """
        TODO:  make it generic to NN or RFC
        Generates a submission file with predictions from the Random Forest (best) Classifier
        Args:
            filename: the name of the file to be submitted, without extension
            model_type: the model to be loaded, must be one of NN or RFC
        """
        if model_type == 'RFC':
            best_estimator = self.rfc_fitted_classifier.best_estimator_
        elif model_type == 'NN':
            best_estimator = self.nn_fitted_classifier.best_estimator_
        else:
            raise Exception('model_type must be one of RFC, NN')

        pred_y = best_estimator.predict(self.input_set).astype(int)
        submission = np.column_stack((np.array(self.input_set.index.values), np.array(pred_y)))
        np.savetxt(filename+'.csv', submission, delimiter=",", header="ID,TARGET")

    def plot_parameters_search (self, model_type, graph=True, display_all_params=True, filename=None, show=True):
        """
        Plots all the parameters after the search.
        Args:
            model_type: the model to be loaded, must be one of NN or RFC
        """
        if model_type=='RFC':
            lp.hpt_search_table_plot(self.rfc_fitted_classifier, self.search_description, graph, display_all_params, filename, show)
        elif model_type=='NN':
            lp.hpt_search_table_plot(self.nn_fitted_classifier, self.search_description, graph, display_all_params, filename, show)
        else:
            raise Exception('model_type must be one of RFC, NN')

    def recursive_RF_(self):
        #Recursively reduce the features of the rfc classifier passed
        if self.rfc_classifier is None:
            raise Exception('There must be a fitted classifier first')

        sel = SelectFromModel(self.rfc_fitted_classifier.best_estimator_, prefit=True)
        selected_feat = self.data_training.columns[(sel.get_support())]


        pass

#TODO: retrieve out of bag samples
#TODO: put random values generation in a function