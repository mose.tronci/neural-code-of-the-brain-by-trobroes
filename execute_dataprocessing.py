import dataProcessor as dp

test_mode = True
pipeline_type = 'log_intervals'
ts_fresh = True
normalise = True
preproc_description = 'log_intervals_normalised_tsfresh'

data_processor = dp.DataProcessor(
    sample_test_size=1000,
    preproc_description=preproc_description,
    test_mode=test_mode,
    tsfresh=ts_fresh,
    normalise=normalise,
    time_conv_fs=100,
    pipeline_type=pipeline_type)

data_processor.transform_data()
data_processor.select_relevant_features()
