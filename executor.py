import modelTrainer as mt
from sklearn.cluster import AffinityPropagation
from sklearn.cluster import KMeans
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
import pickle as pkl
import pandas as pd
import numpy as np
from sklearn.metrics import cohen_kappa_score

np.seterr(divide='ignore', invalid='ignore')
pd.set_option('display.max_colwidth', -1)

"""
Load the data
"""
model_trainer = mt.ModelTrainer(val_size=0.3)

# parameters
iterations = 5
num_clusters = 1

description="""
1) Run a general model to understand features importance, and print the summary results over {} iterations
"""
print(description.format(iterations))

rfc = RandomForestClassifier(
    n_estimators=12,
    criterion='entropy',
    oob_score=False,
    min_samples_leaf=2,
    max_features='sqrt',
    verbose=False
)

model_trainer.run_general_model(
    model=rfc,
    iterations=iterations,
    test_size=0.2,
    verbose=True,
    set_features_importance=True
)

description = """
2) Generate a cluster of neurons, based on the most important features of the general model
"""
print(description)

model_trainer.remove_non_highly_predictive_features(
    num_features=10,
    threshold=0.010,
    verbose=True
)

model_trainer.init_grouped_variables(include_num_records=False)

#clustering_model = AffinityPropagation(convergence_iter=10, damping=0.8, verbose=True)
clustering_model = KMeans(n_clusters=num_clusters, random_state=0)


num_clusters = model_trainer.cluster_neurons(
    clustering_model,
    verbose=False
)
print("\tFound {} clusters".format(num_clusters))

description="""
3) Train a model for all the {} clusters and evaluate the performance over {} iterations
"""
print(description.format(num_clusters, iterations))

clf = MLPClassifier(
    solver='adam',
    alpha=0.0001,
    hidden_layer_sizes=(30, 10, 3),
    learning_rate='adaptive'
)

# clf = RandomForestClassifier(
#     n_estimators=10,
#     criterion='entropy',
#     oob_score=False,
#     min_samples_leaf=2
# )

model_trainer.run_clusters_models(
    model=clf,
    iterations=iterations,
    test_size=0.2,
    restore_features=True,
    verbose=True
)

#models_performance_matrix = model_trainer.test_all_models_on_all_clusters()
#models_performance_matrix.to_csv('models_performance_matrix.csv')

description="""
4) Generate the output
"""
print(description)
model_trainer.predict_input(generate_submission=True, verbose=False)
validation_cohen_kappa = model_trainer.validate_model(verbose=False)
print('The validation data set has a cohen kappa of {:.1%}'.format(validation_cohen_kappa))

