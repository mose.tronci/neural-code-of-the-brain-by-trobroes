import numpy as np
import pandas as pd
import pickle as pkl
from scipy.spatial import distance
from sklearn.base import clone
from sklearn.metrics import accuracy_score
from sklearn.metrics import cohen_kappa_score
from sklearn.model_selection import train_test_split

import dataProcessor as dp

CRED = '\033[91m'
CEND = '\033[0m'


def load(filename):
    return pkl.load(open(filename, 'rb'))


class ModelTrainer:
    """

    """
    test_size = 0.15

    def __init__(self,
                 path_to_data='preprocessing/log_intervals_normalised_tsfresh_relevant',
                 val_size=0.2
                 ):
        self.data_processor = dp.load(path_to_data, stream_to_log_file=False)

        self.X_n = self.data_processor._data['final']['training']['neuron_id']
        self.I_n = self.data_processor._data["final"]["input"]['neuron_id']
        self.X = self.data_processor._data["final"]["training"].drop(['neuron_id'], axis=1)
        self.I = self.data_processor._data["final"]["input"].drop(['neuron_id'], axis=1)
        self.y = self.data_processor._data["source"]["label"]
        self.X, self.X_val, self.y, self.y_val, self.X_n, self.X_n_val = train_test_split(self.X, self.y, self.X_n,
                                                                                          test_size=val_size)
        self.prediction_train = None
        self.prediction_test = None
        self.y_train = None
        self.y_test = None

        # Make sure the data is sorted
        self.X = self.X.sort_index()
        self.X_val = self.X_val.sort_index()
        self.y = self.y.sort_index()
        self.y_val = self.y_val.sort_index()
        self.X_n = self.X_n.sort_index()
        self.X_n_val = self.X_n_val.sort_index()

        self.backup = {
            'X': self.X.copy(deep=True),
            'I': self.I.copy(deep=True),
            'X_val': self.X_val.copy(deep=True)
        }

        self.I_predictions = None
        self.X_val_predictions = None
        self.features_importance = None

        self.X_grouped = None
        self.X_grouped_label = None
        self.I_grouped = None
        self.I_grouped_label = None
        self.X_val_grouped = None
        self.X_val_grouped_label = None

        self.X_clustered_neurons = None
        self.I_clustered_neurons = None
        self.X_val_clustered_neurons = None

        self.clustering_model = None
        self.fitted_clusters_models = None
        self.fitted_clusters_models_performance = None
        self.validation_models_performance = None
        self.centroids_distance = None
        self.validation_cohen_kappa = None

        self.performance_string = """

                Training dataset:
                    cohenkappa mean={:.1%}
                    cohenkappa min={:.1%}
                    cohenkappa max={:.1%}
                    cohenkappa window={:.1%}

                Test dataset:
                    cohenkappa mean={:.1%}
                    cohenkappa min={:.1%}
                    cohenkappa max={:.1%}
                    cohenkappa window={:.1%}
        """
        self.general_model_results = pd.DataFrame()
        self.cluster_models_results = pd.DataFrame()

    def remove_non_highly_predictive_features(self,
                                              num_features=10,
                                              threshold=0.010,
                                              verbose=True):
        """
        Changes the following self variables in place, by removing the features which do not have high predictive power:
        self.X, self.I, self.X_val. Stores the initial values in a backup variable so that they can be restored by the
        _restore_features method

        :param num_features: the maximum number of features to keep
        :param threshold: the minimum value of importance necessary to keep the feature
        """

        important_features = self.features_importance.copy(deep=True)

        # work out the feature type and concatenate to the original array
        features_type_and_params = self.features_importance.feature.str.split('__', expand=True). \
            rename(columns={0: 'model_type',
                            1: 'feature_type',
                            2: 'param0',
                            3: 'param1',
                            4: 'param2',
                            5: 'param3'})
        important_features = pd.concat([important_features, features_type_and_params], axis=1)

        # take the maximum for every feature type
        important_features = important_features.groupby(['feature_type'])['importance', 'feature'].max()
        important_features = important_features.sort_values('importance', ascending=False)
        selected_feature = important_features.query('importance > {}'.format(threshold)).iloc[:num_features, :]
        selected_feature = selected_feature.sort_values('importance', ascending=False)

        if verbose:
            print("Keeping only the following features:")
            print(selected_feature)

        # restore the features so that the method is idempotent
        self._restore_features()

        self.X = self.X.loc[:, selected_feature.feature.values]
        self.I = self.I.loc[:, selected_feature.feature.values]
        self.X_val = self.X_val.loc[:, selected_feature.feature.values]

    def _restore_features(self):
        """Internal utility to restore the data from the backup"""
        self.X = self.backup['X'].copy(deep=True)
        self.I = self.backup['I'].copy(deep=True)
        self.X_val = self.backup['X_val'].copy(deep=True)

    def init_grouped_variables(self, include_num_records=False):
        self.X_grouped, \
        self.X_grouped_label = \
            self._compute_aggregated_features(
                data=pd.concat([self.X_n, self.X], axis=1),
                include_num_records=include_num_records
            )

        self.I_grouped, \
        self.I_grouped_label = \
            self._compute_aggregated_features(
                data=pd.concat([self.I_n, self.I], axis=1),
                include_num_records=include_num_records
            )
        self.I_predictions = None

        self.X_val_grouped, \
        self.X_val_grouped_label = \
            self._compute_aggregated_features(
                data=pd.concat([self.X_n_val, self.X_val], axis=1),
                include_num_records=include_num_records
            )
        self.X_val_predictions = None

    def _compute_aggregated_features(self, data, include_num_records=False):
        """Computes aggregated features of a given pandas, assumes that there is a neuron_id column which becomes
        the label of the return"""
        data = data.copy(deep=True)

        data_mean = data.groupby(['neuron_id']).mean().add_suffix('_mean')

        if include_num_records:
            data_count = data.neuron_id.value_counts().rename('num_records')
            data_count.index.rename('neuron_id', inplace=True)
            data_count = pd.DataFrame(data_count)
            data_mean = pd.concat([data_count, data_mean], sort=False, axis=1)

        data_mean.reset_index(inplace=True)
        data_label = data_mean['neuron_id']
        data_aggregated = data_mean.drop(['neuron_id'], axis=1)

        return data_aggregated, data_label

    def cluster_neurons(self, model, verbose=True):
        clusters = model.fit(self.X_grouped)
        clustered_neurons = pd.concat([
            pd.DataFrame(self.X_grouped_label),
            pd.DataFrame(clusters.labels_).rename(columns={0: 'cluster'}),
            pd.DataFrame(self.X_grouped)
        ], axis=1)

        self.X_clustered_neurons = clustered_neurons

        if verbose:
            print("Identified {} clusters".format(self.X_clustered_neurons['cluster'].max()))

        self.clustering_model = model
        self._calculate_cluster_centroids_distance()
        return self.X_clustered_neurons['cluster'].max() + 1

    def _calculate_cluster_centroids_distance(self):
        num_clusters = self.clustering_model.cluster_centers_.shape[0]
        self.centroids_distance = pd.DataFrame(index=range(num_clusters), columns=range(num_clusters), dtype='float')
        row = 0
        col = 0
        for x in self.clustering_model.cluster_centers_:
            for y in self.clustering_model.cluster_centers_:
                self.centroids_distance.loc[row][col] = distance.euclidean(x, y)
                col = col + 1
            col = 0
            row = row + 1

    def train_and_test_model(self, model, X, y, test_size=0.2, set_features_importance=False, verbose=True):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
        try:
            model.fit(X_train, y_train.values.ravel())
        except ValueError:
            if verbose:
                print(CRED + '\tNot enough samples for fitting the model' + CEND)
            return

        prediction_train = model.predict(X_train)
        cohenkappa_train = cohen_kappa_score(prediction_train, y_train)
        accuracy_train = accuracy_score(prediction_train, y_train)

        prediction_test = model.predict(X_test)
        cohenkappa_test = cohen_kappa_score(prediction_test, y_test)
        accuracy_test = accuracy_score(prediction_test, y_test)

        if verbose:
            print("    Training cohen_kappa_score = {:.1%}".format(cohenkappa_train))
            print("    Training accuracy_score = {:.1%}".format(accuracy_train))
            print("    Test cohen_kappa_score = {:.1%}".format(cohenkappa_test))
            print("    Test accuracy_score = {:.1%}".format(accuracy_test))

        if set_features_importance:
            self.features_importance = pd.concat(
                [pd.DataFrame(model.feature_importances_).rename(columns={0: 'importance'}),
                 pd.DataFrame(X.columns.values).rename(columns={0: 'feature'})], axis=1)

        model_results = {
            'model': model,
            'X_train': X_train,
            'X_test': X_test,
            'y_train': y_train.values.ravel(),
            'y_test': y_test.values.ravel(),
            'prediction_train': prediction_train,
            'cohenkappa_train': cohenkappa_train,
            'prediction_test': prediction_test,
            'cohenkappa_test': cohenkappa_test
        }
        return model_results

    def save(self, filename='models/modelTrainer.pkl'):
        self.data_processor = None
        pkl.dump(self, open(filename, 'wb'))

    def train_models_by_cluster(self, model, restore_features=True, clusters_list=None, test_size=0.2, verbose=True):
        """

        :param model: the model to be used to train the observations in the cluster
        :param restore_features: a flag specifying whether
        :param clusters_list: a list of clusters to train. If not passed, all the clusters are trained
        :param test_size: a number between 0 and 1 representing the observations to use as test
        :param verbose:
        :return:
        """
        if clusters_list is None:
            clusters_list = self.X_clustered_neurons['cluster'].unique()
        clusters_list.sort()

        if restore_features:
            self._restore_features()

        # if the fitted clusters model is as big as the groups in X then the init has taken place, otherwise init
        if self.fitted_clusters_models is None:
            is_init = True
        else:
            is_init = False

        if (is_init is True) or (clusters_list is None):
            if verbose:
                print("Initialising the variables to host the fitted clusters and the predictions")
            self.fitted_clusters_models = {}
            self.fitted_clusters_models_performance = pd.DataFrame(index=clusters_list, columns=[
                'num_neurons',
                'num_observations',
                'cohenkappa_train',
                'cohenkappa_test'
            ])
            self.prediction_train = []
            self.prediction_test = []
            self.y_train = []
            self.y_test = []
            self.fitted_clusters_models_performance.index.name = 'cluster'

        empty_clusters = []

        for c in clusters_list:
            neurons_in_cluster = self.X_clustered_neurons[self.X_clustered_neurons.cluster == c]['neuron_id']
            X_subset = self.X[self.X_n.isin(neurons_in_cluster)]
            y_subset = self.y[self.X_n.isin(neurons_in_cluster)]
            if verbose:
                print("*** Training model on cluster {}, with {} neurons and {} observations ***".format(
                    c,
                    neurons_in_cluster.shape[0],
                    X_subset.shape[0])
                )
            try:
                # need to clone the model otherwise the same one is added to the fitted_clusters_models dict
                cluster_result = self.train_and_test_model(clone(model), X_subset, y_subset, test_size, verbose=verbose)
                self.fitted_clusters_models[c] = cluster_result
                self.fitted_clusters_models_performance.iloc[c] = [
                    neurons_in_cluster.shape[0],
                    X_subset.shape[0],
                    cluster_result['cohenkappa_train'],
                    cluster_result['cohenkappa_test']
                ]
                # todo: if is init we can't extend
                self.prediction_train.extend(cluster_result['prediction_train'])
                self.prediction_test.extend(cluster_result['prediction_test'])
                self.y_train.extend(cluster_result['y_train'])
                self.y_test.extend(cluster_result['y_test'])
            except TypeError:
                self.fitted_clusters_models[c] = {}
                self.fitted_clusters_models_performance.iloc[c] = [
                    neurons_in_cluster.shape[0],
                    X_subset.shape[0],
                    None,
                    None
                ]
                empty_clusters.append(c)
                continue

        # Iterate over the empty clusters, and assign the closest model to the empty fitted_clusters_models
        if verbose:
            print('\n*** Analysing the empty clusters: {} ***'.format(str(empty_clusters)))
        tmp = self.centroids_distance.drop(columns=empty_clusters, axis=1)
        for c in empty_clusters:
            similar_cluster = tmp.iloc[c].idxmin()
            cluster_distance = tmp.loc[c][similar_cluster]
            if verbose:
                print('The cluster most similar to {} is {} (distance={})'.format(c, similar_cluster, cluster_distance))
            self.fitted_clusters_models[c]['model'] = self.fitted_clusters_models[similar_cluster]['model']

        cohenkappa_train = cohen_kappa_score(self.prediction_train, self.y_train)
        cohenkappa_test = cohen_kappa_score(self.prediction_test, self.y_test)
        if verbose:
            print("\n*** Overall results ***")
            print("    Training cohen_kappa_score = {:.1%}".format(cohenkappa_train))
            print("    Test cohen_kappa_score = {:.1%}".format(cohenkappa_test))

        return {'cohenkappa_train': cohenkappa_train, 'cohenkappa_test': cohenkappa_test}

    def predict_input(self, generate_submission=True, filename='models/submission.csv', verbose=True):
        """
        This method clusters the input data and then generates predictions using the models which were trained on the
        individual clusters

        :param generate_submission: if True a sibmission file is generated

        :param verbose:
        """
        self.I_clustered_neurons, self.I_predictions = self._generate_predictions(
            data=self.I,
            data_n=self.I_n,
            data_grouped=self.I_grouped,
            grouped_label=self.I_grouped_label,
            verbose=verbose
        )
        if generate_submission:
            self.I_predictions.to_csv(filename)

    def validate_model(self, verbose=True):
        """This method generates a prediction using the validation data which was set aside from the
        constructor, and returns a cohenkappa score.
        This is useful to verify whether the model is subject to overfit on the test data

        :return: validation_cohen_kappa, the cohen kappa score calculated on the validation data set"""
        self.X_val_clustered_neurons, self.X_val_predictions = self._generate_predictions(
            data=self.X_val,
            data_n=self.X_n_val,
            data_grouped=self.X_val_grouped,
            grouped_label=self.X_val_grouped_label,
            verbose=verbose
        )

        clusters_list = self.X_val_clustered_neurons['cluster'].unique()
        clusters_list.sort()

        self.validation_models_performance = pd.DataFrame(
            index=clusters_list,
            columns=[
                'num_neurons_val',
                'num_observations_val',
                'cohenkappa_val',
                'num_neurons_input',
                'num_observations_input'
            ])

        # Calculate a cohen kappa by cluster
        for c in clusters_list:
            # Get the data for the validation data set
            neurons_in_cluster = self.X_val_clustered_neurons[self.X_val_clustered_neurons.cluster == c]['neuron_id']
            X_subset = self.X_val[self.X_n_val.isin(neurons_in_cluster)]
            y_subset = self.y_val[self.X_n_val.isin(neurons_in_cluster)]
            predictions_subset = self.X_val_predictions[self.X_n_val.isin(neurons_in_cluster)]
            cohen_kappa_subset = cohen_kappa_score(predictions_subset, y_subset)
            I_neurons_in_cluster = self.I_clustered_neurons[self.I_clustered_neurons.cluster == c]['neuron_id']
            self.validation_models_performance.loc[c] = [
                len(neurons_in_cluster),
                len(X_subset),
                cohen_kappa_subset,
                len(self.I_clustered_neurons[self.I_clustered_neurons.cluster == c]['neuron_id']),
                len(self.I[self.I_n.isin(I_neurons_in_cluster)])
            ]
            self.validation_models_performance.index.name = 'cluster'

        if verbose:
            print('Calculating the cohen kappa by cluster and saving in self.validation_models_performance')

        self.validation_models_performance = pd.concat([
            self.fitted_clusters_models_performance.rename(
                columns={
                    'num_neurons': 'num_neurons_train',
                    'num_observations': 'num_observations_train'
                }),
            self.validation_models_performance],
            axis=1)

        self.validation_cohen_kappa = cohen_kappa_score(self.X_val_predictions, self.y_val)

        if verbose:
            print("""The overall cohen kappa score on the validation data set is {:.1%}. 
            Results by clusters are in self.validation_models_results""".format(validation_cohen_kappa))

        return self.validation_cohen_kappa

    def _generate_predictions(self,
                              data,
                              data_n,
                              data_grouped,
                              grouped_label,
                              verbose=True):
        """
        :param data                 the granular / observation level data
                                    e.g. self.I

        :param data_n               the neuron_id at the granular / observation level data
                                    e.g. self.I_n

        :param data_grouped:        the aggregated data
                                    e.g. self.I_grouped

        :param grouped_label:       the label of the aggregated data, i.e. the neuron_id
                                    e.g. self.I_grouped_label

        :param verbose:

        :return:                    clustered_neurons:  the target variable storing the clusters
                                                        predicted on the aggregated data
                                                        e.g. self.I_clustered_neurons

                                    predictions:        the target variable storing the predictions,
                                                        at granular / observation level
                                                        e.g. self.I_predictions
        """

        clusters = self.clustering_model.predict(data_grouped)
        clustered_neurons = pd.concat([
            pd.DataFrame(grouped_label),
            pd.DataFrame(clusters).rename(columns={0: 'cluster'}),
            pd.DataFrame(data_grouped)
        ], axis=1)

        clusters_list = clustered_neurons['cluster'].unique()
        clusters_list.sort()

        predictions = pd.DataFrame()

        for c in clusters_list:
            neurons_in_cluster = clustered_neurons[clustered_neurons.cluster == c]['neuron_id']
            filtered_data = data[data_n.isin(neurons_in_cluster)]
            if verbose:
                print("*** Predicting input for cluster {}, with {} neurons and {} observations ***".format(
                    c,
                    neurons_in_cluster.shape[0],
                    filtered_data.shape[0])
                )
            cluster_predictions = pd.DataFrame(
                index=filtered_data.index,
                data=self.fitted_clusters_models[c]['model'].predict(filtered_data),
                columns=['TARGET']
            )
            predictions = predictions.append(cluster_predictions)

        predictions.sort_index(inplace=True)

        return clustered_neurons, predictions

    def train_model_on_lucky_neuron(self, model, lucky_neuron, test_size=0.2):
        """"Train a model filtering data by a lucky neuron"""
        X_subset = self.X[self.X_n == lucky_neuron]
        y_subset = self.y[self.X_n == lucky_neuron]
        self.train_and_test_model(model, X_subset, y_subset, test_size)

    def run_general_model(self, model, iterations=10, test_size=0.2, verbose=True, set_features_importance=True):
        for i in np.arange(0, iterations):
            model_results = self.train_and_test_model(
                model=model,
                X=self.X,
                y=self.y,
                test_size=test_size,
                set_features_importance=set_features_importance,
                verbose=verbose
            )

            self.general_model_results = self.general_model_results.append(
                pd.DataFrame(np.array(
                    [[i, model_results['cohenkappa_train'], model_results['cohenkappa_test']]]),
                    columns=['index', 'cohenkappa_train', 'cohenkappa_test']
                ))

        print("\tPerformance of the general model over {} iterations:".format(
            iterations) + self.performance_string.format(
            self.general_model_results['cohenkappa_train'].mean(),
            self.general_model_results['cohenkappa_train'].min(),
            self.general_model_results['cohenkappa_train'].max(),
            self.general_model_results['cohenkappa_train'].max() - self.general_model_results['cohenkappa_train'].min(),
            self.general_model_results['cohenkappa_test'].mean(),
            self.general_model_results['cohenkappa_test'].min(),
            self.general_model_results['cohenkappa_test'].max(),
            self.general_model_results['cohenkappa_test'].max() - self.general_model_results['cohenkappa_test'].min()
        )
              )

    def run_clusters_models(self, model, iterations=10, test_size=0.2, clusters_list=None, restore_features=False, verbose=False):
        for i in np.arange(0, iterations):
            model_results = self.train_models_by_cluster(
                model=model,
                test_size=test_size,
                restore_features=restore_features,
                clusters_list=clusters_list,
                verbose=verbose
            )

            self.cluster_models_results = self.cluster_models_results.append(
                pd.DataFrame(np.array(
                    [[i, model_results['cohenkappa_train'], model_results['cohenkappa_test']]]),
                    columns=['index', 'cohenkappa_train', 'cohenkappa_test']
                ))

        print("\tPerformance of the cluster models {} iterations:".format(iterations) + self.performance_string.format(
            self.cluster_models_results['cohenkappa_train'].mean(),
            self.cluster_models_results['cohenkappa_train'].min(),
            self.cluster_models_results['cohenkappa_train'].max(),
            self.cluster_models_results['cohenkappa_train'].max() - self.cluster_models_results[
                'cohenkappa_train'].min(),
            self.cluster_models_results['cohenkappa_test'].mean(),
            self.cluster_models_results['cohenkappa_test'].min(),
            self.cluster_models_results['cohenkappa_test'].max(),
            self.cluster_models_results['cohenkappa_test'].max() - self.cluster_models_results['cohenkappa_test'].min()
        ))

    def _test_clusterid_on_modelclusterid(self, cluster_id, model_id):
        """
        Tests the performance of the model cluster with index id (already trained) using the test data of a given cluster
        :param cluster_id: the index of the cluster to extract a subset of X_test for prediction
        :param model_id:  the index of the (pretrained) cluster model
        :return: the cohen kappa score of the model
        """
        prediction = self.fitted_clusters_models[model_id]['model'].predict(
            self.fitted_clusters_models[cluster_id]['X_test'])
        cohenkappa_train = cohen_kappa_score(prediction, self.fitted_clusters_models[cluster_id]['y_test'])
        return cohenkappa_train

    def test_all_models_on_all_clusters(self, verbose=False):
        """

        :return:
        """
        clusters_list = self.X_clustered_neurons['cluster'].unique()
        clusters_list.sort()

        models_performance_matrix = pd.DataFrame(index=clusters_list, columns=clusters_list)

        for mk in self.fitted_clusters_models.keys():
            for c in self.X_clustered_neurons['cluster'].unique():
                try:
                    models_performance_matrix.loc[mk, c] = self._test_clusterid_on_modelclusterid(c, mk)
                except KeyError:
                    if verbose:
                        print(CRED + 'Could not find data for cluster {} and model {}'.format(c, mk) + CEND)
                    continue

        models_performance_matrix = models_performance_matrix.add_prefix('ck_')
        models_performance_matrix = pd.concat([
            self.fitted_clusters_models_performance.loc[:,
            ['num_neurons', 'num_observations', 'cohenkappa_train', 'cohenkappa_test']],
            models_performance_matrix], axis=1)
        return models_performance_matrix
