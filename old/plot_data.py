from old import libData as ld
from lib import libPlot as lp
import matplotlib.pyplot as plt


T = ld.loadTrainingData()
L = ld.loadTargetData()
neuronID = ld.getRandomNeuronID(T["neuron_id"])

plt.figure(1)

plot_types = ['time', 'int', 'log_time', 'log_int']

for i, p in enumerate(plot_types, start=1):
    plt.subplot(220+i)
    lp.plotHistogram(T, L, p)

plt.figure(2)
plt.subplot(121)
lp.plotTimeSeries(T, L, neuronID)
plt.subplot(122)
lp.plotIntervalSeries(T, L, neuronID)
plt.show()