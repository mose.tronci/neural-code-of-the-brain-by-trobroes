import dataProcessor as dp
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("mode", help="Runs the execution in test or full mode. Usage execute_dataprocessing.py test")
args = parser.parse_args()

if args.mode == 'test':
    test_mode = True
elif args.mode == 'full':
    test_mode = False

# First run the time reconstruction
pipeline_type = 'time_reconstruction'
ts_fresh = False
normalise = False
preproc_description = '{}_tsfresh-{}_norm-{}'.format(pipeline_type, ts_fresh, normalise)

data_processor = dp.DataProcessor(
    sample_test_size=5,
    preproc_description=preproc_description,
    test_mode=test_mode,
    ts_fresh=ts_fresh,
    normalise=normalise,
    time_conv_fs=100,
    pipeline_type=pipeline_type)

data_processor.transform_data()
data_processor.save(close_log=False)

# Now run TS fresh
data_processor.ts_fresh = True
data_processor.transform_tsfresh(finalise=True)
data_processor.save()
