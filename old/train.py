# from sklearn.neural_network import MLPClassifier
# import preprocPipelines as pp
#
# D = pp.logIntervalsPipeline()
# nn = MLPClassifier(hidden_layer_sizes=(49, 25, 12, 12, 5), verbose=True)
# nn.fit(D["X"], D["Y"])

def svm_opt_bins_search(testBins, seed=1):
    from sklearn import svm
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import accuracy_score
    from old import preprocPipelines as pp
    import numpy as np
    import time

    trData = pp.logIntervalsPipeline()

    clf = svm.SVC(gamma='scale', verbose=True)

    tests = np.zeros((len(testBins), 3))

    n = 0

    start = time.time()

    for nBins in testBins:
        np.random.seed(seed)
        synthData = pp.generateExamples(trData, 5000, (0.5, 0.5), nBins)
        clf.fit(synthData["X"], synthData["Y"])
        P = clf.predict(trData["X"])
        ck = cohen_kappa_score(P, trData["Y"])
        a = accuracy_score(P, trData["Y"])

        tests[n, 0] = nBins
        tests[n, 1] = ck
        tests[n, 2] = a

        n += 1

    end = time.time()

    print(end - start)

    return tests

def train_test():
    from sklearn.neural_network import MLPClassifier
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import accuracy_score
    from old import preprocPipelines as pp
    import numpy as np

    np.random.seed(128)

    trData = pp.logIntervalsPipeline()

    prop = 0.7
    tot = 27154

    limIdx = np.intp(np.round(prop * trData["X"].shape[0]))

    synthData = pp.generateExamples(trData, tot - limIdx, (0.0, 1.0), 247)

    perm = np.random.permutation(trData["X"].shape[0])

    X = np.concatenate((trData["X"][perm[:limIdx], :], synthData["X"]))
    Y = np.concatenate((trData["Y"][perm[:limIdx]], synthData["Y"]))

    Xval = trData["X"][perm[limIdx:], :]
    Yval = trData["Y"][perm[limIdx:]]

    perm2 = np.random.permutation(tot)
    X = X[perm2, :]
    Y = Y[perm2]

    clf = MLPClassifier(
        activation='tanh',
        hidden_layer_sizes=(49, 10, 4, 2),
        verbose=True,
        max_iter=1000,
        tol=0.0001,
        alpha=0.1
        )

    clf.fit(X, Y)

    PT  = clf.predict(X)
    ckt = cohen_kappa_score(PT, Y)
    at   = accuracy_score(PT, Y)

    print("Training cohen_kappa_score = " + str(ckt))
    print("Training accuracy_score = " + str(at))

    P   = clf.predict(Xval)
    ck  = cohen_kappa_score(P, Yval)
    a   = accuracy_score(P, Yval)

    print("Validation cohen_kappa_score = " + str(ck))
    print("Validation accuracy_score = " + str(a))

    return X.shape

def train_mlp():
    from sklearn.neural_network import MLPClassifier
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import accuracy_score
    from old import preprocPipelines as pp
    from old import libData as ld
    import numpy as np

    trData = pp.logIntervalsPipeline()

    np.random.seed(128)

    synthData = pp.generateExamples(trData, 250000, (0.5, 0.5), 247)

    mlp = MLPClassifier(
        activation='tanh',
        hidden_layer_sizes=(49, 10, 4, 2),
        verbose=True,
        max_iter=1000,
        tol=0.00001,
        alpha=0.007
        )

    mlp.fit(synthData["X"], synthData["Y"])

    PT  = mlp.predict(synthData["X"])
    ckt = cohen_kappa_score(PT, synthData["Y"])
    at   = accuracy_score(PT, synthData["Y"])

    print("Training cohen_kappa_score = " + str(ckt))
    print("Training accuracy_score = " + str(at))

    P   = mlp.predict(trData["X"])
    ck  = cohen_kappa_score(P, trData["Y"])
    a   = accuracy_score(P, trData["Y"])

    print("Validation cohen_kappa_score = " + str(ck))
    print("Validation accuracy_score = " + str(a))

    inpData = ld.loadInputData()

    X = ld.applyFeaturesNormalization(
        ld.calculateLogIntervals(inpData["time_series"]),
        trData["normData"]
        )

    Y = mlp.predict(X)

    modelData = dict()
    modelData["model"] = mlp
    modelData["cohen_kappa_score"] = ck
    modelData["accuracy_score"] = a
    modelData["inputPrediction"] = Y
    modelData["inputIDs"] = inpData["id"]

    return modelData

def train_logIntFeatures():
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import accuracy_score
    from old import preprocPipelines as pp
    from old import libData as ld
    import numpy as np
    from sklearn import linear_model

    np.random.seed(128)

    trData = pp.logIntervalsFeaturesPipelines(True, False)

    synthData = pp.generateLogIntervalsFeaturesExamples(
        trData,
        1000000,
        0.35,
        5000
        )

    # mlp = MLPClassifier(
    #     activation='tanh',
    #     hidden_layer_sizes=(2, 2, ),
    #     verbose=True,
    #     max_iter=1000,
    #     tol=0.001,
    #     alpha=0.1
    #     )

    # mlp = LogisticRegression(
    #     solver='lbfgs',
    #     tol=1e-4,
    #     verbose=0,
    #     C=1.0,
    #     n_jobs=4
    #     )

    mlp = linear_model.SGDClassifier(
        loss='log',
        class_weight={0.0:1.0, 1.0:1.0},
        max_iter=10000,
        tol=1e-4,
        alpha=1.0,
        n_jobs=4
        )

    # mlp = tree.DecisionTreeClassifier(
    #     random_state=9
    #     )

    mlp.fit(synthData["X"], synthData["Y"])

    PT  = mlp.predict(synthData["X"])
    ckt = cohen_kappa_score(PT, synthData["Y"])
    at   = accuracy_score(PT, synthData["Y"])

    print("Training cohen_kappa_score = " + str(ckt))
    print("Training accuracy_score = " + str(at))

    P   = mlp.predict(trData["X"])
    ck  = cohen_kappa_score(P, trData["Y"])
    a   = accuracy_score(P, trData["Y"])

    print("Validation cohen_kappa_score = " + str(ck))
    print("Validation accuracy_score = " + str(a))

    inpData = pp.load_tsfresh_extracted_features("features/tsfresh-input-logintervals-ordered-and-trimmed-20190310.pckl")

    X = ld.applyFeaturesNormalization(
        inpData.values[:, trData["selIdx"]],
        trData["normData"]
        )

    Y = mlp.predict(X)

    modelData = dict()
    modelData["model"] = mlp
    modelData["cohen_kappa_score"] = ck
    modelData["accuracy_score"] = a
    modelData["inputPrediction"] = Y
    modelData["inputIDs"] = ld.loadInputData()["id"]

    return modelData

def train_logIntFeatures_StatisticMLP():
    from sklearn.neural_network import MLPClassifier
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import accuracy_score
    from old import preprocPipelines as pp
    from old import libData as ld
    import numpy as np
    from sklearn.model_selection import train_test_split
    import copy

    import matplotlib.pyplot as plt

    # np.random.seed(128)

    tries   = 10
    tot     = 30000
    pPos    = 0.17 #np.sum(trData["Y"]) / trData["Y"].shape[0] #0.20

    kMax = 0
    ckLog = np.zeros((tries, ))
    aLog = np.zeros((tries, ))
    models = list()

    model = MLPClassifier(
        activation='tanh',
        solver='adam',
        hidden_layer_sizes=(2, ),
        verbose=True,
        max_iter=1000,
        tol=0.00001,
        warm_start=True,
        n_iter_no_change=2,
        alpha=0.0001
        )

    trData = pp.logIntervalsFeaturesPipelines(True, False)

    realX_train, realX_test, realY_train, realY_test = train_test_split(
        trData["X"],
        trData["Y"],
        test_size=0.2
        )

    statsData = dict()
    statsData["X"] = realX_train
    statsData["Y"] = realY_train
    statsData["normData"] = trData["normData"]

    nPos    = np.sum(realY_train == 1)
    nNeg    = np.sum(realY_train == 0)
    N       = nPos + nNeg

    print("Using " + str(N) + " real training examples in training set")

    Nsynth  = tot - N
    pSynth  = (pPos * tot - nPos) / Nsynth

    print("Using " + str(Nsynth) + " simulated training examples in training set")

    for n in np.arange(tries):

        print("Try " + str(n + 1))

        synthData = pp.generateLogIntervalsFeaturesExamples(
            statsData,
            Nsynth,
            pSynth,
            np.random.randint(4000, high=6000)
            )

        trainX = np.concatenate((realX_train, synthData["X"]))
        trainY = np.concatenate((realY_train, synthData["Y"]))

        # Shuffle rows randomly:
        perm = np.random.permutation(tot)
        trainX = trainX[perm, :]
        trainY = trainY[perm]

        model.fit(trainX, trainY)

        PT  = model.predict(trainX)
        ckt = cohen_kappa_score(PT, trainY)
        at   = accuracy_score(PT, trainY)

        print("Training cohen_kappa_score = " + str(ckt))
        print("Training accuracy_score = " + str(at))

        P   = model.predict(realX_test)
        ck  = cohen_kappa_score(P, realY_test)
        a   = accuracy_score(P, realY_test)

        print("Test cohen_kappa_score = " + str(ck))
        print("Test accuracy_score = " + str(a))

        if ck > kMax:
            modelBest = model
            kMax = ck
            aStore = a

        ckLog[n] = ck
        aLog[n] = a
        models.append(model)

        if n == 0:
            meanModel_coefs = copy.deepcopy(model.coefs_)
            for l in np.arange(len(meanModel_coefs)):
                meanModel_coefs[l] = meanModel_coefs[l] / tries
        else:
            for l in np.arange(len(model.coefs_)):
                meanModel_coefs[l] = meanModel_coefs[l] + copy.deepcopy(model.coefs_[l]) / tries

        print("")

    print("Max Test cohen_kappa_score = " + str(kMax))
    print("Mean Test cohen_kappa_score = " + str(np.mean(ckLog)))

    meanModel = copy.deepcopy(model)
    meanModel.coefs_ = meanModel_coefs

    PM   = meanModel.predict(realX_test)
    ckM  = cohen_kappa_score(PM, realY_test)
    aM   = accuracy_score(PM, realY_test)

    print("Mean Model Test cohen_kappa_score = " + str(ckM))
    print("Mean Model Test accuracy_score = " + str(aM))

    inpData = pp.load_tsfresh_extracted_features("features/tsfresh-input-logintervals-ordered-and-trimmed-20190310.pckl")

    X = ld.applyFeaturesNormalization(
        inpData.values[:, trData["selIdx"]],
        trData["normData"]
        )

    Y = meanModel.predict(X)

    modelData = dict()
    modelData["meanModel"] = meanModel
    modelData["models"] = models
    modelData["model_best"] = modelBest
    modelData["cohen_kappa_score"] = ckLog
    modelData["accuracy_score"] = aLog
    modelData["inputPrediction"] = Y
    modelData["inputIDs"] = ld.loadInputData()["id"]

    plt.plot(ckLog)
    plt.show()

    return modelData

def regressionExp():
    from old import preprocPipelines as pp
    from sklearn.neural_network import MLPRegressor
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import accuracy_score
    import numpy as np

    trData = pp.logIntervalsFeaturesPipelines(True, False)

    realX_train, realX_test, realY_train, realY_test = train_test_split(
        trData["X"],
        trData["Y"],
        test_size=0.2
        )

    model = MLPRegressor(
        activation='tanh',
        solver='adam',
        hidden_layer_sizes=(4, 3, 2, ),
        verbose=True,
        max_iter=1000,
        tol=0.00001,
        warm_start=False,
        n_iter_no_change=2,
        alpha=0.0001
        )

    model.fit(realX_train, realY_train)

    p = model.predict(realX_train)
    y = np.zeros(p.shape)
    y[p >= 0.5] = 1.0
    ck = cohen_kappa_score(y, realY_train)
    a  = accuracy_score(y, realY_train)

    print("Training cohen_kappa_score = " + str(ck))
    print("Training accuracy_score = " + str(a))

    p = model.predict(realX_test)
    y = np.zeros(p.shape)
    y[p >= 0.5] = 1.0

    ck  = cohen_kappa_score(y, realY_test)
    a   = accuracy_score(y, realY_test)

    print("Test cohen_kappa_score = " + str(ck))
    print("Test accuracy_score = " + str(a))

    return model

def train_logIntFeatures_StatisticRandomnForest():
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import accuracy_score
    from old import preprocPipelines as pp
    from old import libData as ld
    import numpy as np
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.ensemble.forest import _generate_unsampled_indices
    from functools import reduce

    # np.random.seed(128)

    tries   = 20

    models = list()
    oobMats = list()
    oobLabs = list()
    oobIdxs = list()

    oob_scores = np.zeros((tries, ))
    synthck = np.zeros((tries, ))

    model = RandomForestClassifier(
        criterion='gini',
        max_depth=None,
        min_samples_split=2,
        n_estimators=30,
        max_features=13,
        oob_score=True,
        n_jobs=4,
        bootstrap=True,
        warm_start=True,
        verbose=0
        )

    n_estimaors_increment = 1

    trData = pp.logIntervalsFeaturesPipelines(True, False)

    synthData = pp.generateLogIntervalsFeaturesExamples(
        trData,
        trData["X"].shape[0],
        np.sum(trData["Y"]) / trData["X"].shape[0],
        np.random.randint(4000, high=6000)
        )

    for n in np.arange(tries):

        print("Try " + str(n + 1))
        print("Model n_estimators = " + str(model.n_estimators))

        model.fit(trData["X"], trData["Y"])

        model.n_estimators = model.n_estimators + n_estimaors_increment

        PT  = model.predict(trData["X"])
        ckt = cohen_kappa_score(PT, trData["Y"])
        at   = accuracy_score(PT, trData["Y"])

        print("Training cohen_kappa_score = " + str(ckt))
        print("Training accuracy_score = " + str(at))

        PS  = model.predict(synthData["X"])
        ckS = cohen_kappa_score(PS, synthData["Y"])
        aS   = accuracy_score(PS, synthData["Y"])

        synthck[n] = ckS

        print("Synth Data cohen_kappa_score = " + str(ckS))
        print("Synth Data accuracy_score = " + str(aS))

        models.append(model)

        print("OOB score = " + str(model.oob_score_))

        isOOBmatInit = False
        n_samples = trData["X"].shape[0]
        for tree in model.estimators_:

            unsampled_indices = _generate_unsampled_indices(
            tree.random_state, n_samples
            )

            oobIdxs.append(unsampled_indices)

        oobIdx = reduce(np.intersect1d, oobIdxs)

        print("There are " + str(oobIdx.shape[0]) + " OOB samples in common to all trees")

        if oobIdx.size != 0:
            oobMat = trData["X"][oobIdx, :]
            oobLab = trData["Y"][oobIdx]
            oobMats.append(oobMat)
            oobLabs.append(oobLab)

            P  = model.predict(oobMat)
            ck = cohen_kappa_score(P, oobLab)
            a   = accuracy_score(P, oobLab)

            print("OOB cohen_kappa_score = " + str(ck))
            print("OOB accuracy_score = " + str(a))

        oob_scores[n] = model.oob_score_

        print("")

    print("Max OOB score = " + str(np.max(oob_scores)))
    print("Mean OOB score = " + str(np.mean(oob_scores)))
    print("Max Synth cohen_kappa_score = " + str(np.max(synthck)))

    bestModel = models[np.argmax(synthck)]

    inpData = pp.load_tsfresh_extracted_features("features/tsfresh-input-logintervals-ordered-and-trimmed-20190310.pckl")

    X = ld.applyFeaturesNormalization(
        inpData.values[:, trData["selIdx"]],
        trData["normData"]
        )

    Y = bestModel.predict(X)

    modelData = dict()
    modelData["models"] = models
    modelData["bestModel"] = bestModel
    modelData["inputPrediction"] = Y
    modelData["inputIDs"] = ld.loadInputData()["id"]

    return modelData

def train_logIntFeatures_SearchRandomnForest():
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.model_selection import GridSearchCV
    from sklearn.metrics import cohen_kappa_score, make_scorer
    import numpy as np
    from old import preprocPipelines as pp
    from old import libData as ld

    model = RandomForestClassifier(
        n_jobs=4
        )

    parameters = {
        'max_features': np.arange(5, 40),
        'n_estimators': np.arange(30, 50)
        }

    grd = GridSearchCV(
        model,
        parameters,
        scoring=make_scorer(cohen_kappa_score),
        cv=3,
        verbose=1,
        n_jobs=4
        )

    trData = pp.logIntervalsFeaturesPipelines(False, False)

    grd.fit(trData["X"], trData["Y"])

    print("Best score is: " + str(grd.grd.best_score_))

    synthData = pp.generateLogIntervalsFeaturesExamples(
        trData,
        trData["X"].shape[0],
        np.sum(trData["Y"]) / trData["X"].shape[0],
        np.random.randint(4000, high=6000)
        )

    P = grd.predict(synthData["X"])
    s = cohen_kappa_score(P, synthData["Y"])

    print("Synth Data cohen_kappa_score = " + str(s))

    inpData = pp.load_tsfresh_extracted_features("features/tsfresh-input-logintervals-ordered-and-trimmed-20190310.pckl")

    X = ld.applyFeaturesNormalization(
        inpData.values[:, trData["selIdx"]],
        trData["normData"]
        )

    Y = grd.predict(X)

    modelData = dict()
    modelData["grd"] = grd
    modelData["inputPrediction"] = Y
    modelData["inputIDs"] = ld.loadInputData()["id"]

    return grd

def get_activations(clf, X):
    import numpy as np

    hidden_layer_sizes = clf.hidden_layer_sizes
    if not hasattr(hidden_layer_sizes, "__iter__"):
        hidden_layer_sizes = [hidden_layer_sizes]
    hidden_layer_sizes = list(hidden_layer_sizes)
    layer_units = [X.shape[1]] + hidden_layer_sizes + [clf.n_outputs_]
    activations = [X]
    for i in range(clf.n_layers_ - 1):
        activations.append(np.empty((X.shape[0], layer_units[i + 1])))
    clf._forward_pass(activations)
    return activations

def multiStageExp():
    from sklearn.neural_network import MLPClassifier
    from sklearn.metrics import cohen_kappa_score
    from old import preprocPipelines as pp
    from old import libData as ld
    import numpy as np
    from sklearn.ensemble import RandomForestClassifier

    mlp = MLPClassifier(
        activation='tanh',
        solver='adam',
        hidden_layer_sizes=(4, ),
        verbose=True,
        max_iter=1000,
        tol=0.00001,
        warm_start=True,
        n_iter_no_change=10,
        alpha=0.0001
        )

    rfr = RandomForestClassifier(
        criterion='gini',
        max_depth=None,
        min_samples_split=2,
        n_estimators=120,
        max_features=2,
        oob_score=False,
        n_jobs=4,
        bootstrap=True,
        warm_start=False,
        verbose=0
        )

    trData = pp.logIntervalsFeaturesPipelines(True, False)

    synthData = pp.generateLogIntervalsFeaturesExamples(
        trData,
        trData["X"].shape[0],
        np.sum(trData["Y"]) / trData["X"].shape[0],
        np.random.randint(4000, high=6000)
        )

    mlp.fit(trData["X"], trData["Y"])

    P = mlp.predict(synthData["X"])
    smlp = cohen_kappa_score(P, synthData["Y"])

    print("MLP Synth Data cohen_kappa_score = " + str(smlp))

    act = get_activations(mlp, trData["X"])

    rfrX = act[len(act) - 2]

    rfr.fit(rfrX, trData["Y"])

    actS = get_activations(mlp, synthData["X"])

    P = rfr.predict(actS[len(act) - 2])
    srfr = cohen_kappa_score(P, synthData["Y"])

    print("Full Model Synth Data cohen_kappa_score = " + str(srfr))

    print("Serial Boost: " + str(srfr / smlp))

    inpData = pp.load_tsfresh_extracted_features("features/tsfresh-input-logintervals-ordered-and-trimmed-20190310.pckl")

    X = ld.applyFeaturesNormalization(
        inpData.values[:, trData["selIdx"]],
        trData["normData"]
        )

    actY = get_activations(mlp, X)

    Y = rfr.predict(actY[len(act) - 2])

    modelData = dict()
    modelData["mlp"] = mlp
    modelData["rfr"] = rfr
    modelData["inputPrediction"] = Y
    modelData["inputIDs"] = ld.loadInputData()["id"]

    return act
