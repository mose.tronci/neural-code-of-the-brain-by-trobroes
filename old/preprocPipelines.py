from old import libData as ld
import numpy as np
import numpy.matlib as npml
import pandas as pd
from tsfresh import extract_features, extract_relevant_features
import pickle


# export_file_path = 'features/'

def describePipelineOutput(data, bins = 512, density = True):
    """Perform Descriptive Statistics of a preprocessing pipeline output.

    Args:
        param1 (dict): dictionary of training data, containing:
        "X", the matrix of training data
        "Y", the array of labels.
        "normData" the dictionary of normalziation data (empty if no
        normalization)

        param2 (int, str, array_like): bins argiment for numpy.histogram
        function, for histogram calculation. Default is 512.

        param2 (bool): density parameter for for numpy.histogram
        function, for histogram calculation. Default is True.

    Returns:
        <class 'dict'> dictionary of preprocessing data, containing:
        "X", stats for the whole X matrix
        "X Y=...", Stats for the X submatrices for the various values of Y.

    """

    # sum(np.diff(Q["histEdges"]) * Q["X"]["histVals"]) is ~ 1
    # If P = np.diff(Q["histEdges"]) * Q["X"]["histVals"]
    # Then this generates 3000 new examples according to P:
    # np.random.choice(Q["histCenters"], size = (3000, 49), p = P)

    desc = dict()

    desc["X"] = dict()
    desc["X"]["histVals"], desc["histEdges"] = np.histogram(
        data["X"],
        bins = bins,
        density = density
        )
    desc["X"]["mean"]   = np.mean(data["X"])
    desc["X"]["std"]    = np.std(data["X"])
    desc["X"]["max"]    = np.amax(data["X"])
    desc["X"]["min"]    = np.amin(data["X"])

    desc["histCenters"] = desc["histEdges"][0:(len(desc["histEdges"]) - 1)] + 0.5 * np.diff(desc["histEdges"])

    for y in np.unique(data["Y"]):
        dicStr = "X Y=" + str(y)

        desc[dicStr] = dict()
        desc[dicStr]["histVals"], _ = np.histogram(
            data["X"][data["Y"] == y, :],
            bins = desc["histEdges"],
            density = density
            )
        desc[dicStr]["mean"]   = np.mean(data["X"][data["Y"] == y, :])
        desc[dicStr]["std"]    = np.std(data["X"][data["Y"] == y, :])
        desc[dicStr]["max"]    = np.amax(data["X"][data["Y"] == y, :])
        desc[dicStr]["min"]    = np.amin(data["X"][data["Y"] == y, :])

    return desc

def generateExamples(data, amount, proportions, bins = 512):

    desc = describePipelineOutput(data, bins)

    labels = np.unique(data["Y"])
    F = data["X"].shape[1]

    for p in range(len(labels)):
        N = np.intp(np.round(amount * proportions[p]))
        P = np.diff(desc["histEdges"]) * desc["X Y=" + str(labels[p])]["histVals"]

        rX = np.random.choice(desc["histCenters"], size = (N, F), p = P)
        rY = labels[p] * np.ones((N,))

        if p == 0:
            X = rX
            Y = rY
        else:
            X = np.concatenate((X, rX))
            Y = np.concatenate((Y, rY))

    perm = np.random.permutation(X.shape[0])

    X = X[perm, :]
    Y = Y[perm]

    exMat = dict()
    exMat["X"] = X
    exMat["Y"] = Y

    return exMat

def generateLogIntervalsFeaturesExamples(data, amount, pPos, bins = 100):
    """ Generate examples for the log interval tsfresh features.

    Args:
        param1 (dict): dictionary of training data, from
        logIntervalsFeaturesPipelines()

        param2 (int): amount of examples to be generated

        param3 (list): proportion of positive examples to be generated

        param4 (int, str, array_like): bins argiment for numpy.histogram
        function, for histogram calculation. Default is 512.

    Returns:
        <class 'dict'> dictionary of training data, containing:
        "X", the matrix of training data
        "Y", the array of labels.

    """

    # Calculating bins limits good for all features
    trMin = np.amin(data["X"])
    trMax = np.amax(data["X"])

    binEdges    = np.arange(trMin, trMax, (trMax - trMin) / (bins + 1))
    binCenters  = binEdges[0:(len(binEdges) - 1)] + 0.5 * np.diff(binEdges)

    nPos = np.intp(np.round(amount * pPos))
    nNeg = amount - nPos

    exMat = dict()
    exMat["X"] = np.zeros((amount, data["X"].shape[1]))
    exMat["Y"] = np.zeros((amount, ))
    exMat["Y"][:nPos] = np.ones((nPos, ))
    exMat["normData"] = data["normData"]

    for c in np.arange(data["X"].shape[1]):

        p, _ = np.histogram(
            data["X"][data["Y"] == 1, c],
            bins=binEdges,
            density = True
            )

        n, _ = np.histogram(
            data["X"][data["Y"] == 0, c],
            bins=binEdges,
            density = True
            )

        # Normalizing counts to provide mass probability
        p = np.diff(binEdges) * p
        n = np.diff(binEdges) * n

        exMat["X"][:nPos, c] = np.random.choice(
            binCenters,
            size=(nPos, ),
            p=p
            )

        exMat["X"][nPos:, c] = np.random.choice(
            binCenters,
            size=(nNeg, ),
            p=n
            )

    # Shuffle rows randomly:
    perm = np.random.permutation(exMat["X"].shape[0])
    exMat["X"] = exMat["X"][perm, :]
    exMat["Y"] = exMat["Y"][perm]

    return exMat


def logIntervalsFeaturesPipelines(normalize = True, selectWows = True):
    """Compute tsfresh features of logarithmic time intervals preprocessing
    pipeline.

    Args:
        param1 (bool): whether apply feature normalization, default = True.
        param2 (bool): whether selecting only wow features, default = True.

    Returns:
        <class 'dict'> dictionary of training data, containing:
        "X", the matrix of training data
        "Y", the array of labels.
        "normData" the dictionary of normalziation data (empty if no
        normalization)

    """

    fName = "features/tsfresh-training-logintervals-ordered-and-trimmed-20190310.pckl"

    tsfreshData = load_tsfresh_extracted_features(fName)

    data                = dict()
    data["Y"]           = ld.loadTargetData()["label"]
    data["normData"]    = dict()

    if not selectWows:
        data["X"] = tsfreshData.values
        data["selIdx"] = np.arange(data["X"].shape[1])
    else:
        data["selIdx"]  = np.array([1,8,21,25,59,60,62,63,64,66,67,75,74,77,78,79,80,81,90,88,91,92,93,98,100,102,103,143,145,148,149,162,169])
        data["X"] = tsfreshData.values[:, data["selIdx"] ]

    if normalize:
        data["normData"] = ld.calculateFeaturesNormalization(data["X"])

        data["X"] = ld.applyFeaturesNormalization(
            data["X"],
            data["normData"]
            )

    return data


def logIntervalsPipeline(normalize = True):
    """Compute logarithmic time intervals preprocessing pipeline.

    Args:
        param1 (bool): whether apply feature normalization, default = True.

    Returns:
        <class 'dict'> dictionary of training data, containing:
        "X", the matrix of training data
        "Y", the array of labels.
        "normData" the dictionary of normalziation data (empty if no
        normalization)

    """

    data                = dict()
    data["Y"]           = ld.loadTargetData()["label"]
    data["normData"]    = dict()

    data["X"] = ld.calculateLogIntervals(
        ld.loadTrainingData()["time_series"]
        )

    if normalize:
        data["normData"] = ld.calculateFeaturesNormalization(data["X"])

        data["X"] = ld.applyFeaturesNormalization(
            data["X"],
            data["normData"]
            )

    return data


def logIntervalsInput(normalize = True):
    """Compute logarithmic time intervals for the input data, required for submissions

    Args:
        param1 (bool): whether apply feature normalization, default = True.

    Returns:
        <class 'dict'> dictionary of training data, containing:
        "X", the matrix of training data
        "id", the row id.
        "normData" the dictionary of normalization data (empty if no
        normalization)

    """

    data                = dict()
    data["normData"]    = dict()

    data["X"] = ld.calculateLogIntervals(
        ld.loadInputData()["time_series"]
        )

    data["id"] = ld.loadInputData()['id']

    if normalize:
        data["normData"] = ld.calculateFeaturesNormalization(data["X"])

        data["X"] = ld.applyFeaturesNormalization(
            data["X"],
            data["normData"]
            )

    return data


def transpose_features_matrix(head=False):
    source_data = ld.loadTrainingData()
    source_data['time_series'] = ld.calculateLogIntervals(source_data['time_series'])
    source_labels = ld.loadTargetData()
    source_labels['label'] = source_labels['label'] == 1

    if head:
        source_data['id'] = source_data['id'][0:head]
        source_data['neuron_id'] = source_data['neuron_id'][0:head]
        source_data['time_series'] = source_data['time_series'][0:head, :]
        source_labels['label'] = source_labels['label'][0:head]

    rows = source_data['time_series'].shape[0]
    cols = source_data['time_series'].shape[1]
    transposed_data = dict()
    transposed_data['id'] = npml.repmat(source_data['id'].reshape((rows, 1)), 1, cols).reshape(rows * cols)
    transposed_data['neuron_id'] = npml.repmat(source_data['neuron_id'].reshape((rows, 1)), 1, cols).reshape(rows * cols)
    transposed_data['time_series'] = source_data['time_series'].reshape((rows * cols))

    #transposed_labels = npml.repmat(source_labels['label'].reshape((rows, 1)), 1, cols).reshape(rows * cols)

    return (transposed_data, source_labels['label'])



def compute_tsfresh_extracted_features(data, labels, filter_relevant_features=True):
    """
    Extracts the features from the training data set, using the tsfresh librarby

    Args:
        filter_relevant_features: True if only the relevant features are extracted, False if all features are extracted

    Returns:
        extracted_features: a panda data frame with the extracted features
    """

    panda_data = pd.DataFrame(data=data)
    panda_labels = pd.Series(data=labels)

    if filter_relevant_features==True:
        extracted_features = extract_relevant_features(panda_data, panda_labels, column_id='id', n_jobs=4)
    else:
        extracted_features = extract_features(panda_data, column_id='id', n_jobs=4)

    return extracted_features

def transpose_input_data(head=None):
    input_data = ld.loadInputData()
    input_data['time_series'] = ld.calculateLogIntervals(input_data['time_series'])

    if head:
        input_data['id'] = input_data['id'][0:head]
        input_data['neuron_id'] = input_data['neuron_id'][0:head]
        input_data['time_series'] = input_data['time_series'][0:head, :]

    rows = input_data['time_series'].shape[0]
    cols = input_data['time_series'].shape[1]
    transposed_data = dict()
    transposed_data['id'] = npml.repmat(input_data['id'].reshape((rows, 1)), 1, cols).reshape(rows * cols)
    transposed_data['neuron_id'] = npml.repmat(input_data['neuron_id'].reshape((rows, 1)), 1, cols).reshape(rows * cols)
    transposed_data['time_series'] = input_data['time_series'].reshape((rows * cols))

    return transposed_data

def preprocess_input_from_given_features(data, config_features):
    panda_data = pd.DataFrame(data=data)
    extracted_features = extract_features(panda_data, column_id='id', n_jobs=4, kind_to_fc_parameters=config_features)
    return extracted_features

def save_tsfresh_extracted_features(extracted_features, filename):
    """
    Saves the extracted features to a file

    Args:
        extracted_features: a panda data frame returned from the extract_features def
        filename: the name of the file, without extension

    Returns: none

    """
    file = open(filename+'.pckl', 'wb')
    pickle.dump(extracted_features,file)
    file.close()

def load_tsfresh_extracted_features(filename):
    """
    Loads features as saved in a given filename

    Args:
        filename: the name of the file where the features have been saved

    Returns:
        extracted_features: a panda data frame with the features extracted from tsfresh

    """
    file = open(filename, 'rb')
    extracted_features = pickle.load(file)
    return extracted_features

def add_random_examples():
    pass
