# Contains useful functions to load and explore the data
import numpy as np

def csv2numpy(fName):
    """Load a CSV file as a numpy array.

    Args:
        fName (str): string containing the path to the csv file.
        Example: "foo/bar/foobar.csv"

    Returns:
        <class 'numpy.ndarray'> numpy array containing the csv data.

    """

    return np.loadtxt(open(fName, "rb"), delimiter=",", skiprows=1)

def loadTrainingData():
    """Load the training data.

    Returns:
        <class 'dict'> dictionary of training data, containing:
        "X", the matrix of spike timestamps
        "Cells" the array of Neuron Cell IDs.
        "IDs" the array of data IDs.

    """

    fileContents = csv2numpy("data/training_un7FCmp.csv")

    data = dict()

    data["id"]          = fileContents[:, 0]
    data["neuron_id"]   = fileContents[:, 1]
    data["time_series"] = fileContents[:, 2:]

    return data

def loadTargetData():
    """Load the target data.

    Returns:
        <class 'dict'> dictionary of target data, containing:
        "label", the array of labels
        "IDs" the array of data IDs.

    """

    fileContents = csv2numpy("data/target_ATndViR.csv")

    data = dict()

    data["id"]      = fileContents[:, 0]
    data["label"]   = fileContents[:, 1]

    return data

def loadInputData():
    """Load the training data.

    Returns:
        <class 'dict'> dictionary of training data, containing:
        "X", the matrix of spike timestamps
        "neuron_id" the array of Neuron Cell IDs.
        "id" the array of data IDs.

    """

    fileContents = csv2numpy("data/input_test_YW07KeM.csv")

    data = dict()

    data["id"]          = fileContents[:, 0]
    data["neuron_id"]   = fileContents[:, 1]
    data["time_series"] = fileContents[:, 2:]

    return data

def dumpPrediction(filename, id, pred):

    np.savetxt(
        filename,
        np.column_stack((id, pred)),
        delimiter=","
        )

def calculateIntervals(X):
    """Given a matrix of time series, calculates the time intervals.

    Args:
        X (numpy.ndarray): numpy matrix of time series.

    Returns:
        <class 'numpy.ndarray'> numpy array of time intervals.

    """

    return np.diff(X, n=1, axis=1)

def calculateLogIntervals(X):
    """Given a matrix of time series, calculates the logarithmic time intervals.

    Args:
        X (numpy.ndarray): numpy matrix of time series.

    Returns:
        <class 'numpy.ndarray'> numpy array of logarithmic time intervals.

    """

    return np.log(calculateIntervals(X))


def calculateLogTimeseries(X):
    """Given a matrix of time series, calculates the logarithmic.

    Args:
        X (numpy.ndarray): numpy matrix of time series.

    Returns:
        <class 'numpy.ndarray'> numpy array of logarithmic time instants.

    """

    return np.log(X)

def calculateFeaturesNormalization(X):
    """Given a matrix, calculate the feature normalziation parameters.

    Args:
        X (numpy.ndarray): numpy matrix.

    Returns:
        <class 'dict'> dictionary of feature normalziation parameters,
        containing:
        "mean", the mean of all matrix rows.
        "std" the sample standard deviation of all matrix rows.

    """

    normData = dict()

    normData["mean"]    = np.mean(X, axis=0)
    normData["std"]     = np.std(X, axis=0)

    return normData

def applyFeaturesNormalization(X, normData):
    """Given a matrix and normalization data, calculate the normalized matrix.

    Args:
        X (numpy.ndarray): numpy matrix of time series.
        normData (dict): dictionary of feature normalziation parameters,
        containing:
            "mean", the mean of all matrix rows.
            "std" the sample standard deviation of all matrix rows.

    Returns:
        <class 'numpy.ndarray'> numpy array of normalized features.

    """

    if not normData:
        return X

    return (X - normData["mean"]) / normData["std"]


def getRandomNeuronID(N):
    """
    Given an array of neuron IDs, returns a neuron ID randomly selected

    Args:
        N(numpy.ndarray): an array of neuron IDs

    Returns:
        random_neuron_id: a neuron ID selected randomly

    """
    random_neuron_id = np.random.choice(N)

    return random_neuron_id
